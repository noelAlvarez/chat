import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppConstants } from '../assets/constants';
import { ChatService } from './services/chat.service';
import { DataStorageService } from './services/data-storage.service';
import { StatesHandlerService } from './services/states-handler.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  getStates: object;
  loading: boolean = true;
  constructor(private _translate: TranslateService,
    private _chat: ChatService,
    private _dataStorage: DataStorageService,
    private _statesHandler: StatesHandlerService) {
    this.loading = true;
    this.appListener();
    //that's our default language
    _translate.setDefaultLang('en');
    //borrar cookie:document.cookie = 'cache=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    this.sendCookie();
  }
  appListener = () => {
    this._dataStorage.appStates.subscribe(() => {
      // if appStates was changed, we need to check if closedState is true or false
      // if closedState is true, we are going to disable all buttons
      if (this._dataStorage.getStates()[AppConstants.CLOSED_CONNECTION]) {
        this._chat.getTranslate('SERVER.MAINTENANCE').then(res => {
          alert(res);
        })
      }
      if (this._dataStorage.getStates()[AppConstants.LOGGED_IN]) {
        this.loading = false;
      }
    });
  }
  switchLanguage(language: string) {
    this._translate.use(language);
  }
  sendCookie = () => {
    if (document.cookie.length > 0) {
      this._chat.sendCookie(document.cookie).then((response) => {
        if (!this._dataStorage.getStates()[AppConstants.CLOSED_CONNECTION]) {
          if (response == "multisession") {
            let status = {
              validity: AppConstants.INVALID,
              value: AppConstants.USER_MULTISESSION
            }
            this._statesHandler.responseHandler(status);
            this.loading = false;
          } else if (typeof response == "object") {
            this._chat.sendMsg(AppConstants.AUTO_LOGIN_COMMAND + response['token']);
            this._dataStorage.setMyNick(response['nick']);
            this._chat.pingConnectionState();
          } else {
            this.loading = false;
          }
        }
      });
    } else {
      this.loading = false;
    }
  }
}