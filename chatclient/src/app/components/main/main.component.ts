import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import { NgModel } from '@angular/forms';
import { HostListener } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { ViewChild, ElementRef } from '@angular/core';
import { AppConstants } from '../../../assets/constants';
import { DataStorageService } from '../../services/data-storage.service';
import { StatesHandlerService } from '../../services/states-handler.service';
import { StreamingService } from '../../services/streaming.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})

@Injectable()
export class MainComponent implements OnInit {

  @ViewChild('inputText') inputText: ElementRef;

  @ViewChild('video') video: ElementRef;

  // that's our model from view, user type messages here
  text: string = '';
  // that's object that will be drawed in our view with all messages and users
  msgObj: Array<object> = [];
  // that's model from view to set error messages
  error: boolean = false;
  // that's model from view to set error message if nick is banned
  nickBanned: boolean = false;
  // show loader when messages are drawing
  loadingDown: boolean;

  loadingUp: boolean;

  scrollToBottom: boolean;

  msgCommand: string;

  privateWindowStyles: boolean = false;

  messagesIndex: any = 0;

  showMessageIndex: boolean;

  fixedScroll: number;

  appUsers: boolean = true;

  showEmojis: boolean = false;

  preset: string = '/assets/preset.jpg';

  cloudinaryImage: any;

  uploadingFile: boolean = false;

  tabToSendMedia: string = '';

  control: boolean = false;

  appHeight: number;

  recording: boolean = false;

  startedStream: string = null;

  constructor(private _chat: ChatService,
    private _dataStorage: DataStorageService,
    private _statesHandler: StatesHandlerService,
    private _streaming: StreamingService) {
    // we build message object for view, taking fields name from constants
    const status = AppConstants.STATUS;
    const messageObj = AppConstants.MESSAGE_OBJECT;
    // we start showing loader
    this.loadingDown = true;
  }

  ngOnInit() {
    // we start listening states
    this.appStatesListener();
    this.privateMsgListener();
    this.filterMessagesListener();
    this.userButtonListener();
    this._dataStorage.setMessagesIndex(0);
    this.showMessageIndex = false;
    this.setAppHeight()
  }

  // we subscribe to changes on appStates
  appStatesListener = () => {
    this._dataStorage.appStates.subscribe(() => {
      // if appStates was changed, we need to check if closedState is true or false
      // if closedState is true, we are going to disable all buttons
      if (this._dataStorage.getStates()[AppConstants.UPDATE_MESSAGES]) {
        if (this._dataStorage.getSelectedTab() == AppConstants.MAIN_ROOM) {
          if (this.msgObj.length > 0) {
            this.getLastMessage();
          } else {
            this.control = true;
            this.updateMainMessages(null);
            this.scrollToBottom = true;
          }
        }
      } else if (this._dataStorage.getStates()[AppConstants.CLOSED_CONNECTION]) {
        this.error = true;
        this.msgObj = [];
        this.showMessageIndex = false;
      } else if (this._dataStorage.getStates()[AppConstants.BANNED]) {
        this.msgObj = [];
        this.showMessageIndex = false;
        this.nickBanned = true;
      }
    });
  }
  // we send message to chat service, and update screen messages because server didn't respond with our own message
  sendMessage() {
    this._chat.sendMsg(this.msgCommand + this.text);
    // we empty input to let user type new messages again
    this.text = "";
    this.showEmojis = false;
    this.inputText.nativeElement.focus();
  }
  // we update screen messages by two ways
  updateMainMessages = (loadingUp) => {
    this.showMessageIndex = false;
    this.msgCommand = '';
    this.privateWindowStyles = false;
    if (loadingUp) {
      this.loadingUp = true;
    } else {
      this.loadingDown = true;
    }
    // if we didn't receive a filteredList, we subscribe to getHistory call
    if (this._dataStorage.getMessageListFiltered().length == 0) {
      this._chat.getHistory.subscribe(
        result => {
          this.messagesIndex = result['data']['index'];
          // we draw history messages into our view
          this.drawMesasges(result['data']['msgs']);
        },
        error => {
          console.log(<any>error);
        }
      );
    } else {
      // if we receive a filteredList, we draw list in our view
      this.drawMesasges(this._dataStorage.getMessageListFiltered());
    }
  }

  privateMsgListener = () => {
    this._dataStorage.updatePrivateHistory.subscribe((value) => {
      if (value) {
        this.loadingDown = true;
        this.msgCommand = AppConstants.PRIVATE_MSG_COMMAND + ' ' + this._dataStorage.getSelectedTab() + ' ';
        this.privateWindowStyles = true;
        if (value[1] == 'last') {
          this.getLastMessage();
        } else {
          this.msgObj = [];
          this.getPrivateHistory();
          this.scrollToBottom = true;
        }
      } else {
        this.msgObj = [];
        this.updateMainMessages(null);
        this.scrollToBottom = true;
      }
      this._dataStorage.setMessagesIndex(0);
      this.showMessageIndex = false;
      this.inputText.nativeElement.focus();
    })
  }

  getPrivateHistory = () => {
    this._chat.getPrivateHistory.subscribe(
      result => {
        if (result['data']['index']) {
          this.messagesIndex = result['data']['index'];
        } else {
          this.messagesIndex = 'max';
        }
        // we draw history messages into our view
        this.drawMesasges(result['data']['msgs']);
      },
      error => {
        console.log(<any>error);
      }
    );
  }
  getLastMessage = () => {
    this.drawMesasges(this._dataStorage.getLastMsg());
  }

  drawMesasges = (msgs) => {
    if (msgs) {
      if (!Array.isArray(msgs)) {
        this.msgObj.push(msgs);
      } else {
        this.msgObj = msgs;
      }
    }
    this.messagesReady();
  }

  scrollHandler = (event) => {
    if (!this.fixedScroll) {
      let height = document.querySelector('.mainScreen').scrollHeight;
      let scrollPos = document.querySelector('.mainScreen').scrollTop;
      if (height - scrollPos < 800) {
        this.scrollToBottom = true;
      } else {
        this.scrollToBottom = false;
      }
    }
  }
  //
  // that function makes loader hide
  messagesReady = () => {
    let that = this;
    setTimeout(() => {
      // we hide loader
      that.loadingUp = false;
      that.loadingDown = false;
      if (this.scrollToBottom) {
        document.querySelector('.mainScreen').scrollTop = document.querySelector('.mainScreen').scrollHeight;
      } else if (this.fixedScroll) {
        document.querySelector('.mainScreen').scrollTop = (document.querySelector('.mainScreen').scrollHeight - this.fixedScroll);
      }
      that.showMessageIndex = true;
      that.fixedScroll = null;
    }, 60)
  }
  pad(s, width, character) {
    return new Array(width - s.toString().length + 1).join(character) + s;
  }
  formatDate(date) {
    var maxDate = new Date(date);
    var maxDateFormatted =
      this.pad(maxDate.getDate(), 2, '0') +
      '/' +
      this.pad(maxDate.getMonth() + 1, 2, '0') +
      '/' +
      maxDate.getFullYear() +
      ' ' +
      new Date(date).toString().substr(16, 5);
    return maxDateFormatted;
  }
  getMoreMessages = () => {
    this.fixedScroll = document.querySelector('.mainScreen').scrollHeight;
    this._dataStorage.setMessagesIndex(this._dataStorage.getMessagesIndex() + 1);
    if (this._dataStorage.getSelectedTab() == AppConstants.MAIN_ROOM) {
      this.updateMainMessages('loadingUp');
    } else {
      this.loadingUp = true;
      this.getPrivateHistory();
    }
  }
  filterMessagesListener = () => {
    this._dataStorage.messageListFiltered.subscribe(() => {
      if (this.control) {
        this.updateMainMessages(null);
      }
    });
  }

  userButtonListener = () => {
    this._dataStorage.appUsers.subscribe(() => {
      let oldScreenHeight = document.querySelector('.mainScreen').scrollHeight;
      let oldScreenScrollPos = document.querySelector('.mainScreen').scrollTop;
      this.appUsers = this._dataStorage.getAppUsers();
      setTimeout(() => {
        let actualScreenHeight = document.querySelector('.mainScreen').scrollHeight;
        let actualScreenScrollPos = (actualScreenHeight * oldScreenScrollPos) / oldScreenHeight;
        document.querySelector('.mainScreen').scrollTop = actualScreenScrollPos;
      }, 100)
    })
  }
  addEmoji = (event) => {
    this.text = this.text + event.emoji.native;
    var that = this;
    setTimeout(() => {
      that.inputText.nativeElement.focus();
    }, 50);
  }
  emojiDisplay = () => {
    this.showEmojis = !this.showEmojis;
    this.inputText.nativeElement.focus();
  }
  trackByFn(msg) {
    return msg.timestamp;
  }
  setAppHeight = () => {
    this.appHeight = window.innerHeight;
    window.scrollTo(0, window.innerHeight);
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    setTimeout(() => {
      let height;
      if (event) {
        height = event.target.innerHeight;
      } else {
        height = this.appHeight;
      }
      if (height == this.appHeight) {
        this.inputText.nativeElement.blur();
        window.scrollTo(0, 0);
      }
    }, 300);
  }
  callStreamingService = (streamType: string) => {
    this._streaming.setVideo(this.video.nativeElement);
    this._streaming.setStreamType(streamType);
    this._streaming.setStreamClient(this._dataStorage.getSelectedTab());
    this.startedStream = 'ask';
    this._streaming.init();
    this.streamingListener();
  }
  streamingListener = () => {
    let that = this;
    this._streaming.streamIn.subscribe(function (value) {
      if (!value) {
        this.unsubscribe();
        that.streamEnd();
      }
    })
  }
  streamEnd = () => {
    this.startedStream = null;
    this._streaming.streamEnd.next();
  }
  acceptStream = (config) => {
    let clientID = config[0];
    let streamType = config[1];
    this._streaming.setVideo(this.video.nativeElement);
    this._streaming.setStreamType(streamType);
    this._streaming.setStreamClient(clientID);
    this.startedStream = 'join';
    this._streaming.init();
    this.streamingListener();
  }
  declineStream = (streamQuery) => {
    streamQuery.expired = true;
    this._chat.declineStreamQuery(streamQuery.id);
    this._streaming.setStreamClient(streamQuery.clientID);
    this.startedStream = 'decline';
  }
}
