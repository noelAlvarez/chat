import { Component, OnInit } from '@angular/core';
import { DataStorageService } from '../../services/data-storage.service';
import { StatesHandlerService } from '../../services/states-handler.service';
import { AppConstants } from '../../../assets/constants';

@Component({
  selector: 'app-chat-controls',
  templateUrl: './chat-controls.component.html',
  styleUrls: ['./chat-controls.component.css']
})
export class ChatControlsComponent implements OnInit {
  sound: boolean = true;
  users: boolean = true;
  closed: boolean = false;
  privateMsg: boolean = false;
  settings: boolean = false;

  constructor(private _dataStorage: DataStorageService,
    private _statesHandler: StatesHandlerService) { }

  ngOnInit() {
    this.appStatesListener();
    this.appUsersListener();
  }
  // we subscribe to changes on appStates
  appStatesListener = () => {
    this._dataStorage.appStates.subscribe(() => {
      // if appStates was changed, we need to check if closedState is true or false
      // if closedState is true, we are going to disable all buttons
      if (this._dataStorage.getStates()[AppConstants.CLOSED_CONNECTION]) {
        this.closed = true;
      }
    });
  }
  updateSound = () => {
    this.sound = !this.sound;
    this._dataStorage.setAppSound(this.sound);
  }
  updateUsers = () => {
    this.users = !this.users;
    this._dataStorage.setAppUsers(this.users);
  }
  appUsersListener = () => {
    this._dataStorage.updatePrivateHistory.subscribe((value) => {
      if (value) {
        this.privateMsg = true;
        this.users = false;
        this._dataStorage.setAppUsers(this.users);
      } else {
        this.privateMsg = false;
      }
    })
  }
  settingsPanel = () => {
    this.settings = !this.settings;
  }
  // we set changeNick state to true, this will open login window
  changeNick = () => {
    // that's our global handler of server or app response to all components
    this._statesHandler.responseHandler({
      validity: AppConstants.VALID,
      value: AppConstants.CHANGING_NICKNAME
    });
  }
  showUserID = () => {
    this._statesHandler.responseHandler({
      validity: AppConstants.VALID,
      value: AppConstants.SHOW_USERID
    });
  }
  exit = () => {
    this._statesHandler.responseHandler({
      validity: AppConstants.VALID,
      value: AppConstants.LOG_OFF
    });
  }
}
