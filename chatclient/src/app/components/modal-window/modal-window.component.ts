import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { ViewChild, ElementRef } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { ComponentInitializerService } from '../../services/component-initializer.service';
import { AppConstants } from '../../../assets/constants';
import { StatesHandlerService } from '../../services/states-handler.service';
import { DataStorageService } from '../../services/data-storage.service';

@Component({
  selector: 'app-modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.css']
})

export class ModalWindowComponent implements OnInit {

  @ViewChild('inputText') inputText: ElementRef;
  @ViewChild('inputUserID') inputUserID: ElementRef;
  // that's our model from view, user writes here a nick
  nick: string;

  user: string;
  // that's our command to specify a nick
  nickCommand: string = AppConstants.NICK_COMMAND;
  // that's our model from view, to show different nick errors
  nickInUseError: boolean;
  nickSameError: boolean;
  nickUnderscoreError: boolean;
  nickMultisession: boolean;
  // that's our state handler
  appStates: object = {};
  // that's our title in our view
  title: string = '';
  // that's our input in our view
  inputPlaceholder: string = '';
  // that's our submit button in our view
  submitButtonText: string = '';
  // that's our closeButton in our view
  closeButton: boolean = false;

  allowLoginSubmit: boolean = false;

  showModal: boolean = false;

  insertSavedUser: boolean = false;

  showSavedUser: boolean = false;

  invalidUser: boolean = false;

  logoff: boolean = false;

  userID: string;

  genericModal: boolean = false;

  genericModalClose: boolean = false;

  genericModalContent: string = '';

  constructor(private _chat: ChatService,
    private _componentInit: ComponentInitializerService,
    private _statesHandler: StatesHandlerService,
    private _dataStorage: DataStorageService) {
    this.appStatesListener();
  }

  ngOnInit() { }

  initComponent = () => {

    // that's our object to store particular data of our desired component, to bind it to this (that's a generic component)
    this._componentInit.mainInit().then((initObj) => {
      if (initObj) {
        this.title = initObj['title'] || this.title;
        this.nickCommand = initObj['nickCommand'] || this.nickCommand;
        this.submitButtonText = initObj['submitButton'] || this.submitButtonText;
        this.inputPlaceholder = initObj['inputPlaceholder'] || this.inputPlaceholder;
        this.closeButton = initObj['allowCloseButton'] || this.closeButton;
        this.allowLoginSubmit = initObj['allowLoginSubmit'] || this.allowLoginSubmit;
        this.resetErrors();
      }
    });
  }
  // that function is called when click on close button
  closeModal = () => {
    this._dataStorage.initInvalidStates();
    this.showModal = false;
    this.insertSavedUser = false;
    this.showSavedUser = false;
    this.logoff = false;
    this.genericModal = false;
    this.genericModalClose = false;
  }

  submit = () => {
    // if our particular component needs a particular submit function we control it with a boolean
    if (this.allowLoginSubmit) {
      // if we receive no result, nick is not in use and we let user log in or change his nickname
      this._chat.sendMsg(this.nickCommand + this.nick);
      this._dataStorage.setMyNick(this.nick);
      this._chat.pingConnectionState();
      this.nick = '';
      this.showModal = false;
    }
  }

  sendSavedUser = () => {
    if (this.user == document.cookie) {
      this.resetErrors();
      this.nickSameError = true;
      return;
    }
    this._chat.sendCookie(this.user).then((response) => {
      if (!this._dataStorage.getStates()[AppConstants.CLOSED_CONNECTION]) {
        if (response == "multisession") {
          this._statesHandler.responseHandler({
            validity: AppConstants.INVALID,
            value: AppConstants.USER_MULTISESSION
          });
        } else if (typeof response == "object") {
          this._chat.sendMsg(AppConstants.AUTO_LOGIN_COMMAND + response['token']);
          this._dataStorage.setMyNick(response['nick']);
          this._chat.pingConnectionState();
          this.insertSavedUser = false;
          this._dataStorage.setCookie(this.user);
        } else {
          this.resetErrors();
          this.invalidUser = true;
        }
      }
    });
  }

  appStatesListener = () => {
    this._dataStorage.appStates.subscribe(() => {
      // if appStates was changed, we need to check if closedState is true or false
      // if closedState is true, we are going to disable all buttons
      this.resetErrors();
      if (this._dataStorage.getStates()[AppConstants.NICK_IN_USE]) {
        this.nickInUseError = true;
        this.showModalWindow();
      } else if (this._dataStorage.getStates()[AppConstants.NICK_SAME]) {
        this.nickSameError = true;
        this.showModalWindow();
      } else if (this._dataStorage.getStates()[AppConstants.NICK_UNDERSCORE]) {
        this.nickUnderscoreError = true;
        this.showModalWindow();
      } else if (this._dataStorage.getStates()[AppConstants.USER_MULTISESSION]) {
        this.nickMultisession = true;
      } else if (this._dataStorage.getStates()[AppConstants.CONNECTING] ||
        this._dataStorage.getStates()[AppConstants.CHANGING_NICKNAME]) {
        //first we init generic component with our particular component requirements
        this.initComponent();
        this.showModalWindow();
      } else if (this._dataStorage.getStates()[AppConstants.LOGGED_IN] ||
        this._dataStorage.getStates()[AppConstants.CLOSED_CONNECTION]) {
        this.showModal = false;
      } else if (this._dataStorage.getStates()[AppConstants.SHOW_USERID]) {
        this.initUserIDModal();
      } else if (this._dataStorage.getStates()[AppConstants.LOG_OFF]) {
        this.logoff = true;
      } else if (this._dataStorage.getStates()[AppConstants.GENERIC_MODAL]) {
        this.manageGenericModal();
      }
    });
  }
  selectAllContent($event) {
    $event.target.select();
  }
  initUserIDModal = () => {
    this.showSavedUser = true;
    this.userID = document.cookie;
    setTimeout(() => {
      this.inputUserID.nativeElement.focus();
    }, 100)
  }
  preventWriting = () => {
    return false;
  }
  exit = () => {
    this._dataStorage.setCookie('');
    window.location.href = "http://google.com"
  }
  resetErrors = () => {
    this.nickInUseError = false;
    this.nickSameError = false;
    this.nickUnderscoreError = false;
    this.nickMultisession = false;
    this.invalidUser = false;
  }
  manageGenericModal = () => {
    let modal = this._dataStorage.getModalContent();
    this.genericModalContent = modal[0];
    if (modal[1]) {
      this.genericModalClose = true;
    } else {
      this.genericModal = true;
    }
  }
  showModalWindow = () => {
    this.showModal = true;
    setTimeout(() => {
      // we focus on input field to let it ready to type message again
      if (this.inputText) {
        this.inputText.nativeElement.focus();
      }
    }, 50);
  }
  ngOnDestroy() {
    this._dataStorage.appStates.unsubscribe();
  }
}