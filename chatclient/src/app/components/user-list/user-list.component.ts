import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { AppConstants } from '../../../assets/constants';
import { DataStorageService } from '../../services/data-storage.service';
import { StatesHandlerService } from '../../services/states-handler.service';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {


  userList: Array<object>;

  FROM: string = AppConstants.FROM;

  // that's model from view to disable buttons
  closed: boolean = false;

  // that's our state object
  appStates: object = {};

  online: boolean = true;
  offline: boolean = false;
  action: string = '';
  myNick: string;
  filtered:boolean = false;

  constructor(private _chat: ChatService,
    private _dataStorage: DataStorageService,
    private _statesHandler: StatesHandlerService) { }

  ngOnInit() {
    this.appStatesListener();
    this.nickListener();
  }

  // we subscribe to changes on appStates
  appStatesListener = () => {
    this._dataStorage.appStates.subscribe(() => {
      // if appStates was changed, we need to check if closedState is true or false
      // if closedState is true, we are going to disable all buttons
      if (this._dataStorage.getStates()[AppConstants.CLOSED_CONNECTION]) {
        this.closed = true;
      } else if (this._dataStorage.getStates()[AppConstants.MESSAGE_RESPONSE]) {
        this.updateUserList();
      }
    });
  }

  // That's filter function that can filter specific user or filter all users again
  filterUser = (user) => {
    // we receive messages history first
    this._chat.getHistory.subscribe(
      result => {
        // if we receive some user in our function, we filter his messages
        if (user) {
          this._dataStorage.setMessageListFiltered(
            result['data']['msgs'].filter(function (field) {
              return field[this.FROM] == user
            }.bind(this)));
            this.filtered = true;
          // if we didn't receive user in our function, we send complete history
        } else {
          this.filtered = false;
          this._dataStorage.setMessageListFiltered([]);
        }
        this.action = '';
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  nickListener = () => {
    this._dataStorage.myNick.subscribe(() => {
      this.myNick = this._dataStorage.getMyNick();
    });
  }

  // we subscribe to userList call to receive list of users
  updateUserList = () => {
    this._chat.getUserList.subscribe(
      result => {
        // we send to our user-list component, list of users, to let component draw on his view
        var data = result['data'].sort(function (a, b) {
          return a['nick'].localeCompare(b['nick']);
        });
        this.userList = data;
        this.getIgnoredStates();
      },
      error => {
        console.log(<any>error);
      }
    );
  }
  privateMsgUser = (user) => {
    if (this.myNick == user) {
      return;
    } else {
      this._dataStorage.setSelectedTab(user);
      this._chat.sendMsg(AppConstants.PRIVATE_MSG_COMMAND + ' ' + user + ' ');
      this.action = '';
    }
  }

  drawIgnoredStates = (nickList) => {
    this._chat.getIgnoredStates(this.myNick, nickList).then((ignoredList) => {
      for (let i = 0; i < this.userList.length; i++) {
        this.userList[i]['ignored'] = ignoredList['data'][i];
        this.updateIgnoredList(this.userList[i]['nick'], this.userList[i]['ignored']);
      }
      let status = {
        validity: AppConstants.VALID,
        value: AppConstants.UPDATE_MESSAGES
      }
      this._statesHandler.responseHandler(status);
    })
  }

  getIgnoredStates = () => {
    let nickList = [];
    for (let i = 0; i < this.userList.length; i++) {
      nickList.push(this.userList[i]['nick']);
    }
    this.drawIgnoredStates(nickList);
  }

  showActions = (nick) => {
    this.action == nick ? this.action = '' : this.action = nick;
  }

  ignoreUser = (ignoredUser) => {
    this._chat.ignoreUser(ignoredUser).then(() => {
      for (let i = 0; i < this.userList.length; i++) {
        if (this.userList[i]['nick'] == ignoredUser) {
          this.userList[i]['ignored'] = !this.userList[i]['ignored'];
          this.updateIgnoredList(this.userList[i]['nick'], this.userList[i]['ignored']);
        }
      };
    });
    this.action = '';
  }
  updateIgnoredList = (nick, state) => {
    let nickExists = this._dataStorage.myIgnoredList.indexOf(nick);
    if (state && nickExists == -1) {
      this._dataStorage.myIgnoredList.push(nick);
    } else if (!state && nickExists != -1) {
      this._dataStorage.myIgnoredList.splice(nick, 1);
    }
  }
}