import { Component, Input } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { DataStorageService } from '../../services/data-storage.service';
import { StatesHandlerService } from '../../services/states-handler.service';
import { AppConstants } from '../../../assets/constants';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { FileUploader } from 'ng2-file-upload';
import { ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-send-media',
  templateUrl: './send-media.component.html',
  styleUrls: ['./send-media.component.css']
})
export class SendMediaComponent {

  @ViewChild('inputFile') inputFile: ElementRef;

  @Input("error") error: boolean;

  uploadingFile: boolean = false;

  uploader: CloudinaryUploader = new CloudinaryUploader(
    new CloudinaryOptions({ cloudName: 'dybxr57eg', uploadPreset: 'rqzq6tko' })
  );

  constructor(private _chat: ChatService,
    private _dataStorage: DataStorageService,
    private _statesHandler: StatesHandlerService) {
    this.uploadFileListener();
  }

  loadImage = (img) => {
    img.target.src = img.target.attributes['data-url'].value;
  }
  checkFile = (file) => {
    let src = file.target.value;
    let size = file.target.files[0].size * 10e-7;
    let extension = file.target.value.substr(file.target.value.lastIndexOf('.') + 1).toLowerCase();
    if (size && size > 50) {
      this._chat.getTranslate('APP.WARNING.AUDIO_FILE_SIZE').then((text) => {
        this._dataStorage.setModalContent('<p>' + text + '</p>', true);
        this._statesHandler.responseHandler({
          validity: AppConstants.VALID,
          value: AppConstants.GENERIC_MODAL
        });
      });
    } else {
      switch (extension) {
        case ('mp3'): case ('mp4'): case ('jpg'): case ('png'): case ('webp'): case ('gif'):
        case ('bmp'): case ('ico'): case ('jpeg'): case ('webm'): case ('ogv'): case ('flv'):
        case ('wma'): case ('ogg'): case ('aiff'): case ('m4a'): case ('flac'): case ('wav'):
        case ('pdf'):
          this.uploadFile();
          break;
        default:
          this._chat.getTranslate('APP.WARNING.AUDIO_FILE_FORMAT').then((text) => {
            this._dataStorage.setModalContent('<p>' + text + '</p>', true);
            this._statesHandler.responseHandler({
              validity: AppConstants.VALID,
              value: AppConstants.GENERIC_MODAL
            });
          });
      }
    }
  }
  uploadFile = () => {
    this.uploadingFile = true;
    this.uploader.uploadAll();

  }
  uploadFileListener = () => {
    //Override onSuccessItem to retrieve the imageId
    this.uploader.onSuccessItem = (item: any, response: string, status: number, headers: any): any => {
      this._chat.formatSend(response);
      this.uploadingFile = false;
    };
  }

}
