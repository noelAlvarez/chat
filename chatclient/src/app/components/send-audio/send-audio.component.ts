import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as RecordRTC from 'recordrtc';
import { ChatService } from '../../services/chat.service';
import { DataStorageService } from '../../services/data-storage.service';
import { StatesHandlerService } from '../../services/states-handler.service';
import { AppConstants } from '../../../assets/constants';

@Component({
  selector: 'app-send-audio',
  templateUrl: './send-audio.component.html',
  styleUrls: ['./send-audio.component.css']
})
export class SendAudioComponent {

  recordRTC: any;

  recording: boolean = false;

  @Input('text') text: boolean;

  @Input('error') error: boolean;

  @Input('loadingUp') loadingUp: boolean;

  @Input('loadingDown') loadingDown: boolean;

  @Output()
  recordState = new EventEmitter();

  constructor(private _chat: ChatService,
    private _dataStorage: DataStorageService,
    private _statesHandler: StatesHandlerService) { }

  startAudio = () => {
    this.recording = true;
    this.recordState.emit(this.recording);
    navigator.getUserMedia({
      audio: true,
      video: false
    }, (mediaStream) => {
      this.recordRTC = RecordRTC(mediaStream);
      var fiveMinutes = 5 * 1000 * 60;
      this.recordRTC.setRecordingDuration(fiveMinutes, () => {
        this._chat.getTranslate('APP.WARNING.AUDIO_MAX_DURATION').then((text) => {
          this._dataStorage.setModalContent('<p>' + text + '</p>', true);
          this._statesHandler.responseHandler({
            validity: AppConstants.VALID,
            value: AppConstants.GENERIC_MODAL
          });
        })
      });
      this.recordRTC.startRecording();
    }, () => {
      this.recording = false;
      this.recordState.emit(this.recording);
      this._chat.getTranslate('APP.WARNING.AUDIO_NOT_AVAILABLE').then((text) => {
        this._dataStorage.setModalContent('<p>' + text + '</p>', true);
        this._statesHandler.responseHandler({
          validity: AppConstants.VALID,
          value: AppConstants.GENERIC_MODAL
        });
      })
    });
  }
  stopAudio = () => {
    this.recording = false;
    this.recordState.emit(this.recording);
    if (this.recordRTC.state == "recording") {
      this.recordRTC.stopRecording((audioURL) => {
        this.readData();
      });
    } else {
      this.readData();
    }
  }
  cancelAudio = () => {
    this.recording = false;
    this.recordState.emit(this.recording);
    if (this.recordRTC.state == "recording") {
      this.recordRTC.stopRecording((audioURL) => {
        this.recordRTC.clearRecordedData();
      });
    } else {
      this.recordRTC.clearRecordedData();
    }
  }
  readData = () => {
    let blob = this.recordRTC.getBlob();
    let reader = new FileReader();
    reader.readAsDataURL(blob);
    reader.onloadend = () => {
      let file = {
        url: reader.result,
        size: blob.size
      };
      this.recordRTC.clearRecordedData();
      this.ajaxUpload(file);
    }
  }
  ajaxUpload = (file) => {
    const data = new FormData();
    data.append('file', file.url);
    data.append('upload_preset', "rqzq6tko");
    data.append('api_key', 'PbvuM1Eo4tE8vZ9OPA6CzXlS8Ks');
    //data.append('api_key', apiKey);
    this._chat.uploadFile(data).then((response) => {
      response['format'] = 'mp3';
      this._chat.formatSend(JSON.stringify(response));
    }).catch(function (e) {
      console.log(e);
    });
  }
}
