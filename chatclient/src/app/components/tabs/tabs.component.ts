import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Observable, Subject, BehaviorSubject } from 'rxjs/Rx';
import { DataStorageService } from '../../services/data-storage.service';
import { AppConstants } from '../../../assets/constants'
import { ChatService } from '../../services/chat.service';
import { ViewChild, ElementRef } from '@angular/core';
import { StreamingService } from '../../services/streaming.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {

  closed: boolean = false;
  selected: string = AppConstants.MAIN_ROOM;
  msgUpdates: Array<string> = [];
  tabList: Array<string> = ['Main'];
  mainRoom: string = AppConstants.MAIN_ROOM;
  audio;
  appUsers: boolean = true;
  toggleList: boolean = false;
  hasUpdatesMobileBorder: boolean = false;
  @ViewChild('mobileTab') mobileTab: ElementRef;

  constructor(private _dataStorage: DataStorageService, private _chat: ChatService,
    private _streaming: StreamingService) { }

  ngOnInit() {
    this._dataStorage.setSelectedTab(this.mainRoom);
    this.appStatesListener();
    this.tabListener();
    this.userButtonListener();
    this.audio = new Audio('assets/message.mp3');
  }

  // we subscribe to changes on appStates
  appStatesListener = () => {
    this._dataStorage.appStates.subscribe(() => {
      // if appStates was changed, we need to check if closedState is true or false
      // if closedState is true, we are going to disable all buttons
      if (this._dataStorage.getStates()[AppConstants.CLOSED_CONNECTION]) {
        this.closed = true;
      } else if (this._dataStorage.getStates()[AppConstants.PRIVATE_MESSAGE]) {
        this.manageTabs();
      } else if (this._dataStorage.getStates()[AppConstants.UPDATE_TABS]) {
        this.updateOldTabs(this._dataStorage.getOldNick(), this._dataStorage.getNewNick())
      }
    });
  }

  updateOldTabs = (oldNick, newNick) => {
    let indexTab = this.tabList.indexOf(oldNick);
    if (indexTab != -1) {
      this.tabList[indexTab] = newNick;
      let indexHasUpdates = this.msgUpdates.indexOf(oldNick);
      if (indexHasUpdates != -1) {
        this.msgUpdates[indexHasUpdates] = newNick;
      }
      if (this._dataStorage.getSelectedTab() == oldNick) {
        this._dataStorage.setSelectedTab(newNick);
        this._dataStorage.updatePrivateHistory.next(true);
      }
    }
  }

  tabListener = () => {
    this._dataStorage.selectedTab.subscribe((value) => {
      if (!this.isActive(value)) {
        this.selectTab(value);
      }
    });
  }

  selectTab = (item) => {
    if (!this.isActive(item)) {
      this._dataStorage.setMessagesIndex(0);
      this.selected = item;
      this.manageList(item);
      this.msgUpdates.splice(item, 1);
      this._dataStorage.setSelectedTab(item);
      if (item !== this.mainRoom) {
        this._dataStorage.setUserFrom(item);
        this._dataStorage.updatePrivateHistory.next(true);
      } else {
        this._dataStorage.updatePrivateHistory.next(false);
      }
    }
  }
  isActive = (item) => {
    return this.selected === item;
  }
  hasUpdates = (item) => {
    return this.msgUpdates.indexOf(item) !== -1 ? true : false;
  }

  manageTabs = () => {
    let userFrom = this._dataStorage.getUserFrom();
    let userTo = this._dataStorage.getUserTo();
    let item;
    if (this._dataStorage.getMyNick() === userFrom) {
      item = userTo;
    } else {
      item = userFrom;
      if (this._dataStorage.getAppSound()) {
        this.audio.play();
      }
    }
    if (this.tabList.indexOf(item) === -1) {
      this.tabList.push(item);
      this.msgUpdates.push(item);
      this.hasUpdatesMobileBorder = true;
    } else if (this.isActive(item)) {
      if (this._dataStorage.getUserFrom() == this._dataStorage.getMyNick()) {
        this._dataStorage.setUserTo(item);
      } else {
        this._dataStorage.setUserFrom(item);
      }
      this._dataStorage.updatePrivateHistory.next([true, 'last']);
    } else {
      this.msgUpdates.push(item);
      this.hasUpdatesMobileBorder = true;
    }
  }
  closeTab = (item) => {
    if (item != 'Main') {
      let index = this.tabList.indexOf(item);
      this.removeTab(item, index);
      this.selectTab(this.tabList[index - 1] || AppConstants.MAIN_ROOM);
    }
    this._streaming.streamEnd.next();
  }
  userButtonListener = () => {
    this._dataStorage.appUsers.subscribe(() => {
      this.appUsers = this._dataStorage.getAppUsers();
    })
  }
  manageList = (item) => {
    let index = this.tabList.indexOf(item);
    if (window.getComputedStyle(this.mobileTab.nativeElement, null).getPropertyValue('display') != 'none') {
      if (index != -1) {
        this.removeTab(item, index);
      }
      this.tabList.unshift(item);
    } else {
      if (index == -1) {
        this.tabList.push(item);
      }
    }
  }
  removeTab = (item, index) => {
    this.tabList.splice(index, 1);
  }
  toggleTabs = () => {
    this.toggleList = !this.toggleList;
    this.hasUpdatesMobileBorder = false;
  }
}
