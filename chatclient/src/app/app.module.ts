import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { Ng2CloudinaryModule } from 'ng2-cloudinary';
import { FileUploadModule } from 'ng2-file-upload';
import { ClipboardModule } from 'ngx-clipboard';
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { ModalWindowComponent } from './components/modal-window/modal-window.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { WebsocketService } from './services/websocket.service';
import { ChatService } from './services/chat.service';
import { DataStorageService } from './services/data-storage.service';
import { ComponentInitializerService } from './services/component-initializer.service';
import { StatesHandlerService } from './services/states-handler.service';
import { AppConstants } from '../assets/constants';
import { ChatControlsComponent } from './components/chat-controls/chat-controls.component';
import { SendAudioComponent } from './components/send-audio/send-audio.component';
import { SendMediaComponent } from './components/send-media/send-media.component';
import { StreamingDirective } from './directives/streaming.directive';
import { StreamingService } from './services/streaming.service';

const appRoutes: Routes =
  [
    {
      path: '',
      component: AppComponent
    },
    {
      path: '**',
      component: AppComponent
    }
  ];

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ModalWindowComponent,
    UserListComponent,
    TabsComponent,
    ChatControlsComponent,
    SendAudioComponent,
    SendMediaComponent,
    StreamingDirective,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    PickerModule,
    Ng2CloudinaryModule,
    FileUploadModule,
    ClipboardModule
  ],
  providers: [
    WebsocketService,
    ChatService,
    DataStorageService,
    ComponentInitializerService,
    StatesHandlerService,
    StreamingService,
    AppConstants
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
