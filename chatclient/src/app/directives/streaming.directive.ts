import { Directive, Input } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import { StreamingService } from '../services/streaming.service';
import { DataStorageService } from '../services/data-storage.service';
import { ChatService } from '../services/chat.service';

@Directive({
  selector: '[appStreaming]'
})
export class StreamingDirective {

  clientID: string;

  @Input('appStreaming')
  set initialize(value: string) {
    this.clientID = this._streaming.getStreamClient();
    if (value == "ask") {
      this.ask();
    } else if (value == "join") {
      this.join();
    } else if (value == "decline") {
      this.decline();
    }
  }

  peer: any;

  conn: any;

  browser = <any>navigator;

  video: any;

  subscription;

  myID = new Subject();

  constructor(private _streaming: StreamingService,
    private _dataStorage: DataStorageService,
    private _chat: ChatService) {
  }

  init = () => {
    this.browser.getUserMedia = (this.browser.getUserMedia || this.browser.webkitGetUserMedia || this.browser.mozGetUserMedia || this.browser.msGetUserMedia);
    this.streamingListener();
    this.video = this._streaming.getVideo();
    this.onOpen(); this.onClose(); this.connected();
  }

  ask = () => {
    this.myID.subscribe((value) => {
      this._chat.sendMsg('/cam ' + this.clientID + ' ' + document.cookie + ' ' + value);
    })
    this.peer = this.createPeer();
    this.init();
  }

  join = () => {
    this.peer = this.createPeer();
    this.init();
    this.connect(this.clientID);
  }

  createPeer = () => {
    return new Peer({
      host: location.hostname,
      port: 9000,
      path: '/peerConn',
      debug: 3,
      config: {
        'iceServers': [
          { url: 'stun:stun1.l.google.com:19302' },
          {
            url: 'turn:numb.viagenie.ca',
            credential: 'muazkh', username: 'webrtc@live.com'
          }
        ]
      }
    })
  }

  decline = () => {
    this.join();
    this.end();
  }

  end = () => {
    this.subscription.unsubscribe();
    if (this.conn) {
      this.conn.close();
    }
  }
  connect = (id) => {
    this.conn = this.peer.connect(id);
    let streamType = this._streaming.getStreamType();
    this.streamConnect(streamType);
  }

  streamingListener = () => {
    this.subscription = this._streaming.streamIn.subscribe((value) => {
      if (!value) {
        this.end();
      }
    })
  }

  connected = () => {
    this.peer.on('connection', function (conn) {
      console.log();
      conn.on('data', function (data) {
        console.log(data);
      });
    })
  }

  onOpen = () => {
    this.peer.on('open', (id) => {
      this.myID.next(id);
      console.log();
      //this.conn.send('Message from that id');
    });
  }

  onClose = () => {
    this.peer.on('close', () => {
      this.end();
    });
  }

  callingMe = () => {
    this.peer.on('call', (call) => {
      this.browser.getUserMedia({ video: true, audio: true }, (stream) => {
        call.answer(stream);
        this.bindToVideoTag(call);
      }, function (err) {
        console.log('Failed to get stream', err);
      })
    })
  }

  streamConnect(streamType) {
    let mediaObj = { video: false, audio: true };
    if (streamType == "cam") {
      mediaObj.video = true;
    }
    this.browser.getUserMedia(mediaObj, (stream) => {
      var call = this.peer.call(this.clientID, stream);
      this.bindToVideoTag(call);
    }, function (err) {
      console.log('Failed to get stream', err);
    })
  }

  bindToVideoTag = (call) => {
    call.on('stream', (remotestream) => {
      this.video.src = URL.createObjectURL(remotestream);
      this.video.play();
    })
  }
}
