import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs/Rx';
import { DataStorageService } from '../services/data-storage.service'

@Injectable()
export class StreamingService {

  streamEnd = new Subject();
  streamType: string;
  streamUser: string;
  streamIn = new BehaviorSubject(true);
  video: any;
  streamClient: string;
  subscription;

  constructor(private _dataStorage: DataStorageService) {
  }

  init = () => {
    this.streamOpenFn();
    this.streamEndFn();
    this.streamIn.next(true);
  }

  end = () => {
    this.subscription.unsubscribe();
  }

  streamOpenFn = () => {
    this.streamUser = this._dataStorage.getSelectedTab();
  }
  streamEndFn = () => {
    this.subscription = this.streamEnd.subscribe(() => {
      if (this.streamUser = this._dataStorage.getSelectedTab()) {
        if (this.streamIn) {
          this.streamIn.next(false);
          this.end();
        }
      }
    })
  }
  setVideo = (video) => {
    this.video = video;
  }
  getVideo = () => {
    return this.video;
  }
  setStreamType = (streamType) => {
    this.streamType = streamType;
  }
  getStreamType = () => {
    return this.streamType;
  }
  getStreamClient = () => {
    return this.streamClient;
  }
  setStreamClient = (streamClient) => {
    this.streamClient = streamClient;
  }
}
