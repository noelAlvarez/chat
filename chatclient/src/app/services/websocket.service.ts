import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as Rx from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { AppConstants } from '../../assets/constants';

@Injectable()

export class WebsocketService {

  // Our socket connection
  private socket;

  retryInterval;

  constructor() { }
  // that's our main method to connect with websocket
  connect(): Rx.Subject<string> {
    // First we connect with websocket
    this.socket = new WebSocket(environment.ws_url);

    // we listen socket response to catch messages
    let observable = new Observable(observer => {
      this.socket.onmessage = (event => {
        observer.next(event.data);
        clearInterval(this.retryInterval);
      });
      this.socket.onerror = (event => {
        let obj = {};
        obj[AppConstants.STATUS] = {
          validity: AppConstants.INVALID,
          value: AppConstants.ERROR_STATE
        };
        observer.next(JSON.stringify(obj));
      });
      this.socket.onclose = (event => {
        let obj = {};
        obj[AppConstants.STATUS] = {
          validity: AppConstants.INVALID,
          value: AppConstants.CLOSED_CONNECTION
        };
        observer.next(JSON.stringify(obj));
        // we try to reconnect
        this.retryConnection();
      });
    });

    // We define our Observer which will listen to messages
    // from our other components and send messages back to our
    // socket server whenever the `next()` method is called.
    let observer = {
      next: (data: Object) => {
        this.socket.send(data);
      }
    };

    // we return our Rx.Subject which is a combination
    // of both an observer and observable.
    return Rx.Subject.create(observer, observable);
  }

  // for now websockets is giving problems, so we refresh page as temporal solution
  retryConnection = () => {
    let that = this;
    this.retryInterval = setTimeout(function () {
      location.reload();
    }, 5000);
  }
}
