import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs/Rx';
import { AppConstants } from '../../assets/constants';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataStorageService {

  // that's our state handler, we use it to know about socket connection
  appStates = new BehaviorSubject({});
  // we store privateMessage user here to know who is sending private message to us.
  userFrom: string;
  // we store privateMessage user here to know user what we send private message.
  private userTo: string;

  myNick = new BehaviorSubject('');

  updatePrivateHistory = new Subject();

  selectedTab = new BehaviorSubject('');

  oldNick: string;

  newNick: string;

  messagesIndex: number = 0;

  messageListFiltered = new BehaviorSubject([]);

  appSound: boolean = true;

  appUsers = new BehaviorSubject(true);

  myIgnoredList: Array<object> = [];

  modalContent: Array<any>;

  lastMsg: string;

  constructor(private http: HttpClient) {
    //states of app
    this.initValidStates();
    this.initInvalidStates();
    this.setStates(this.appStates);
  }

  getStates = () => {
    return this.appStates.getValue();
  }

  setStates = (states) => {
    this.appStates.next(states);
  }
  initValidStates = () => {
    this.appStates[AppConstants.CONNECTING] = false;
    this.appStates[AppConstants.LOGGED_IN] = false;
    this.appStates[AppConstants.PRIVATE_MESSAGE] = false;
    this.appStates[AppConstants.LOGIN_RESPONSE] = false;
    this.appStates[AppConstants.CHANGING_NICKNAME] = false;
    this.appStates[AppConstants.MESSAGE_RESPONSE] = false;
    this.appStates[AppConstants.UPDATE_TABS] = false;
    this.appStates[AppConstants.SEND_COOKIE] = false;
    this.appStates[AppConstants.UPDATE_MESSAGES] = false;
    this.appStates[AppConstants.SHOW_USERID] = false;
    this.appStates[AppConstants.LOG_OFF] = false;
    this.appStates[AppConstants.GENERIC_MODAL] = false;
  }
  initInvalidStates = () => {
    this.appStates[AppConstants.CLOSED_CONNECTION] = false;
    this.appStates[AppConstants.NICK_UNDERSCORE] = false;
    this.appStates[AppConstants.NICK_SAME] = false;
    this.appStates[AppConstants.NICK_IN_USE] = false;
    this.appStates[AppConstants.USER_MULTISESSION] = false;
    this.appStates[AppConstants.BANNED] = false;
  }
  setLastMsg = (lastMsg) => {
    this.lastMsg = lastMsg;
  }
  getLastMsg = () => {
    return this.lastMsg;
  }
  getUserFrom = () => {
    return this.userFrom;
  }
  setUserFrom = (userFrom) => {
    this.userFrom = userFrom;
  }
  getUserTo = () => {
    return this.userTo;
  }
  setUserTo = (userTo) => {
    this.userTo = userTo;
  }
  getMyNick = () => {
    return this.myNick.getValue();
  }
  setMyNick = (myNick) => {
    this.myNick.next(myNick);
  }
  getSelectedTab = () => {
    return this.selectedTab.getValue();
  }
  setSelectedTab = (selectedTab) => {
    this.selectedTab.next(selectedTab);
  }
  setOldNick = (oldNick) => {
    this.oldNick = oldNick;
  }
  setNewNick = (newNick) => {
    this.newNick = newNick;
  }
  getOldNick = () => {
    return this.oldNick;
  }
  getNewNick = () => {
    return this.newNick;
  }
  setMessagesIndex = (index) => {
    this.messagesIndex = index;
  }
  getMessagesIndex = () => {
    return this.messagesIndex;
  }
  setMessageListFiltered = (filteredList) => {
    this.messageListFiltered.next(filteredList);
  }
  getMessageListFiltered = () => {
    return this.messageListFiltered.getValue();
  }
  getAppSound = () => {
    return this.appSound;
  }
  setAppSound = (appSound) => {
    this.appSound = appSound;
  }
  getAppUsers = () => {
    return this.appUsers.getValue();
  }
  setAppUsers = (appUsers) => {
    this.appUsers.next(appUsers);
  }
  setCookie = (cookie) => {
    document.cookie = document.cookie + ';expires=Thu, 01 Jan 1970 00:00:01 GMT';
    document.cookie = '';
    document.cookie = cookie;
  }
  getModalContent = () => {
    return this.modalContent;
  }
  setModalContent = (modalContent: string, closeBtn) => {
    this.modalContent = [modalContent, closeBtn];
  }
}