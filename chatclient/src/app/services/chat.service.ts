import { Injectable } from '@angular/core';
import { WebsocketService } from './websocket.service';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs/Rx';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';
import { AppConstants } from '../../assets/constants';
import { StatesHandlerService } from './states-handler.service';
import { DataStorageService } from './data-storage.service';

@Injectable()
export class ChatService {
  // that's our listener to socket response
  messages: Subject<any>;
  // that's our global state handler
  appStates: object = {};

  bannedStatus: object = {
    validity: AppConstants.INVALID,
    value: AppConstants.BANNED
  };

  // Our constructor calls our wsService connect method
  constructor(private wsService: WebsocketService,
    private http: HttpClient,
    private _statesHandler: StatesHandlerService,
    private _dataStorage: DataStorageService,
    private _translate: TranslateService) {
    // first we start listening socket changes
    this.initSocketConnection();
    // that makes socket alive with heroku cloud
    this.pingServer();
    this.pingConnectionState();

  }
  // if we receive response from server we process data with responseHandler function
  initSocketConnection = () => {
    this.messages = <Subject<any>>this.wsService.connect();

    this.messages.subscribe((response) => {
      // we try to extract response
      let responseObj = JSON.parse(response);
      if (responseObj.msg) {
        this._dataStorage.setLastMsg(responseObj.msg);
      }
      if (responseObj.toUser) {
        // fromUser can be myself if i'm writing private message, or can be user writing me
        this._dataStorage.setUserFrom(responseObj.fromUser);
        // toUser can be a user if i'm writing private message, or can be myself if someone is writing me
        this._dataStorage.setUserTo(responseObj.toUser);
      } else if (responseObj.status.oldNick) {
        // fromUser can be myself if i'm writing private message, or can be user writing me
        this._dataStorage.setOldNick(responseObj.status.oldNick);
        // toUser can be a user if i'm writing private message, or can be myself if someone is writing me
        this._dataStorage.setNewNick(responseObj.status.newNick);
      } else if (responseObj.status.cookie) {
        this._dataStorage.setCookie(responseObj.status.cookie);
        return;
      }
      // that's our global handler of server or app response to all components
      this._statesHandler.responseHandler(responseObj.status);
    })
  }

  // Our simplified interface for sending
  // messages back to our server
  sendMsg(msg) {
    if (!this._dataStorage.getStates()[AppConstants.BANNED]) {
      this.messages.next(msg);
    }
  }
  // we use this call to get history of messages
  getHistory = new Observable(observer => {
    if (!this._dataStorage.getStates()[AppConstants.BANNED]) {
      let bodyObj = {};
      bodyObj['ignoreds'] = this._dataStorage.myIgnoredList;
      bodyObj['index'] = this._dataStorage.getMessagesIndex();
      this.http.post(environment.http_url + AppConstants.HISTORY_URL, bodyObj).subscribe((result) => {
        if (result[AppConstants.ERROR_STATE] == AppConstants.BANNED) {
          this._statesHandler.responseHandler(this.bannedStatus);
        } else {
          observer.next(result);
        }
      });
    }
  });
  getPrivateHistory = new Observable(observer => {
    if (!this._dataStorage.getStates()[AppConstants.BANNED]) {
      let bodyObj = {};
      bodyObj['fromUser'] = this._dataStorage.getMyNick();
      bodyObj['index'] = this._dataStorage.getMessagesIndex();
      if (this._dataStorage.getMyNick() == this._dataStorage.getUserFrom()) {
        bodyObj['toUser'] = this._dataStorage.getUserTo();
      } else {
        bodyObj['toUser'] = this._dataStorage.getUserFrom();
      }
      bodyObj['cache'] = document.cookie;
      this.http.post(environment.http_url + AppConstants.HISTORY_PRIVATE_URL, bodyObj).subscribe((result) => {
        if (result[AppConstants.ERROR_STATE] == AppConstants.BANNED) {
          this._statesHandler.responseHandler(this.bannedStatus);
        } else {
          observer.next(result);
        }
      });
    }
  });

  getLastMessage = new Observable(observer => {
    if (!this._dataStorage.getStates()[AppConstants.BANNED]) {
      this.http.get(environment.http_url + AppConstants.LAST_MESSAGE_URL).subscribe((result) => {
        if (result[AppConstants.ERROR_STATE] == AppConstants.BANNED) {
          this._statesHandler.responseHandler(this.bannedStatus);
        } else {
          observer.next(result);
        }
      });
    }
  });

  getLastPrivate = new Observable(observer => {
    if (!this._dataStorage.getStates()[AppConstants.BANNED]) {
      let bodyObj = {};
      bodyObj['fromUser'] = this._dataStorage.getMyNick();
      if (this._dataStorage.getMyNick() == this._dataStorage.getUserFrom()) {
        bodyObj['toUser'] = this._dataStorage.getUserTo();
      } else {
        bodyObj['toUser'] = this._dataStorage.getUserFrom();
      }
      bodyObj['cache'] = document.cookie;
      this.http.post(environment.http_url + AppConstants.LAST_PRIVATE_MESSAGE_URL, bodyObj).subscribe((result) => {
        if (result[AppConstants.ERROR_STATE] == AppConstants.BANNED) {
          this._statesHandler.responseHandler(this.bannedStatus);
        } else {
          observer.next(result);
        }
      });
    }
  });
  // we use this call to get user list
  getUserList = new Observable(observer => {
    if (!this._dataStorage.getStates()[AppConstants.BANNED]) {
      this.http.get(environment.http_url + AppConstants.USERLIST_URL).subscribe((result) => {
        if (result[AppConstants.ERROR_STATE] == AppConstants.BANNED) {
          this._statesHandler.responseHandler(this.bannedStatus);
        } else {
          observer.next(result);
        }
      });
    }
  });

  getToken = () => {
    if (!this._dataStorage.getStates()[AppConstants.BANNED]) {
      return new Promise((resolve, reject) => {
        this.http.get(environment.http_url + AppConstants.GET_TOKEN_URL).subscribe((result) => {
          if (result[AppConstants.ERROR_STATE] == AppConstants.BANNED) {
            this._statesHandler.responseHandler(this.bannedStatus);
            resolve(null);
          } else {
            resolve(result['data']);
          }
        });
      });
    }
  }

  ignoreUser = (ignoredUser) => {
    if (!this._dataStorage.getStates()[AppConstants.BANNED]) {
      let bodyObj = {};
      bodyObj['ignoredUser'] = ignoredUser;
      bodyObj['cache'] = document.cookie;
      return new Promise((resolve, reject) => {
        this.http.post(environment.http_url + AppConstants.IGNORED_USER_URL, bodyObj).subscribe((result) => {
          if (result[AppConstants.ERROR_STATE] == AppConstants.BANNED) {
            this._statesHandler.responseHandler(this.bannedStatus);
            resolve(null);
          } else {
            resolve(result['data']);
          }
        });
      });
    }
  }

  getIgnoredStates = (user, userList) => {
    if (!this._dataStorage.getStates()[AppConstants.BANNED]) {
      let bodyObj = {};
      bodyObj['user'] = user;
      bodyObj['userList'] = userList;
      bodyObj['cache'] = document.cookie;
      return new Promise((resolve, reject) => {
        this.http.post(environment.http_url + AppConstants.GET_IGNORED_STATE_URL, bodyObj).subscribe((result: Array<boolean>) => {
          if (result[AppConstants.ERROR_STATE] == AppConstants.BANNED) {
            this._statesHandler.responseHandler(this.bannedStatus);
            resolve(false);
          } else {
            resolve(result);
          }
        });
      });
    }
  }

  sendCookie = (cookie) => {
    if (!this._dataStorage.getStates()[AppConstants.BANNED]) {
      let bodyObj = {};
      bodyObj['cookie'] = cookie;
      return new Promise((resolve, reject) => {
        this.http.post(environment.http_url + AppConstants.CHECK_COOKIE_URL, bodyObj).subscribe((result) => {
          resolve(result['data']);
        });
      });
    }
  }
  confirmConnectionState = () => {
    if (!this._dataStorage.getStates()[AppConstants.BANNED]) {
      let bodyObj = {};
      bodyObj['nick'] = this._dataStorage.getMyNick();
      this.http.post(environment.http_url + AppConstants.CHECK_CONNECTION_STATE_URL, bodyObj).subscribe();
    }
  }

  uploadFile = (form) => {
    if (!this._dataStorage.getStates()[AppConstants.BANNED]) {
      return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest();
        request.addEventListener('load', (event) => {
          resolve(JSON.parse(event.target['responseText']));
        });
        request.addEventListener('error', (event) => {
          reject('Error with upload');
        })
        request.open('POST', 'https://api.cloudinary.com/v1_1/dybxr57eg/raw/upload', true);
        request.send(form);
      });
    }
  }

  formatSend = (response) => {
    let msgCommand = '';
    if (this._dataStorage.getSelectedTab() != AppConstants.MAIN_ROOM) {
      msgCommand = AppConstants.PRIVATE_MSG_COMMAND + ' ' + this._dataStorage.getSelectedTab() + ' ';
    }
    let res: any = JSON.parse(response);
    if (res.format == 'mp3' || res.format == 'ogg' || res.format == 'wav'
      || res.format == 'm4a' || res.format == 'flac' || res.format == 'acc'
      || res.format == 'wma') {
      this.sendMsg(msgCommand + res.url + '?audioFile');
    } else if (res.resource_type == 'image') {
      this.sendMsg(msgCommand + res.url + '?imgFile');
    } else if (res.resource_type == 'video') {
      this.sendMsg(msgCommand + res.url + '?videoFile');
    }
  }

  declineStreamQuery = (streamID) => {
    if (!this._dataStorage.getStates()[AppConstants.BANNED]) {
      let bodyObj = {};
      bodyObj['streamID'] = streamID;
      this.http.post(environment.http_url + AppConstants.DECLINED_STREAM_URL, bodyObj).subscribe();
    }
  }

  getTranslate = (text) => {
    return new Promise((resolve, reject) => {
      console.log()
      this._translate.get(text).subscribe(res => {
        resolve(res);
      });
    });
  }

  pingServer = () => {
    setInterval(() => {
      this.sendMsg('');
    }, 40000);
  }
  pingConnectionState = () => {
    this.confirmConnectionState();
    setInterval(() => {
      this.confirmConnectionState();
    }, 120000);
  }
}