import { Injectable } from '@angular/core';
import { DataStorageService } from '../services/data-storage.service';
import { AppConstants } from '../../assets/constants';
import { Observable, Subject } from 'rxjs/Rx';
import { ChatService } from './chat.service';

@Injectable()
export class ComponentInitializerService {

  getStates: object;
  initObject: object;
  constructor(private _dataStorage: DataStorageService,
    private _chat: ChatService) {
  }
  // we check actual app state to know what kind of component we need to initialize
  mainInit = () => {
    return new Promise((resolve, reject) => {
      let componentToInitialize = this._dataStorage.getStates();
      if (componentToInitialize[AppConstants.CONNECTING]) {
        this.initLogin().then((res) => {
          resolve(res);
        });
      } else if (componentToInitialize[AppConstants.CHANGING_NICKNAME]) {
        this.initNickChange().then((res) => {
          resolve(res);
        });
      }
    });
  };
  // we initialize login type of component with particular stuff like texts, buttons
  initLogin = () => {
    return new Promise((resolve, reject) => {
      let title: string;
      let submitButton: string;
      let inputPlaceholder: string;
      let p1 = this._chat.getTranslate('APP.LOGIN.TITLE');
      let p2 = this._chat.getTranslate('APP.LOGIN.SUBMIT_BUTTON.TEXT');
      let p3 = this._chat.getTranslate('APP.LOGIN.INPUT_PLACEHOLDER');
      let p4 = this._chat.getTranslate('APP.USER.INSERT_USER_LINK');
      Promise.all([p1, p2, p3, p4]).then((values) => {
        resolve({
          title: values[0],
          submitButton: values[1],
          inputPlaceholder: values[2],
          nickCommand: AppConstants.NICK_COMMAND,
          allowLoginResponse: true,
          allowLoginSubmit: true,
          allowCloseButton: false
        })
      })
    })
  };
  // we initialize nickChange type of component with particular stuff like texts, buttons
  initNickChange = () => {
    return new Promise((resolve, reject) => {
      let title: string;
      let submitButton: string;
      let inputPlaceholder: string;
      let p1 = this._chat.getTranslate('APP.NICK_CHANGE.TITLE');
      let p2 = this._chat.getTranslate('APP.NICK_CHANGE.SUBMIT_BUTTON.TEXT');
      let p3 = this._chat.getTranslate('APP.NICK_CHANGE.INPUT_PLACEHOLDER');
      let p4 = this._chat.getTranslate('APP.USER.INSERT_USER_LINK');
      Promise.all([p1, p2, p3, p4]).then((values) => {
        resolve({
          title: values[0],
          submitButton: values[1],
          inputPlaceholder: values[2],
          nickCommand: AppConstants.NICK_COMMAND,
          allowLoginResponse: true,
          allowLoginSubmit: true,
          allowCloseButton: true
        })
      })
    })
  };
}