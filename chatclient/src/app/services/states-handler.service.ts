import { Injectable } from '@angular/core';
import { DataStorageService } from '../services/data-storage.service';
import { AppConstants } from '../../assets/constants';

@Injectable()
export class StatesHandlerService {

  appStates: object;

  constructor(private _dataStorage: DataStorageService) { }

  // that's our global handler of server response to all components
  responseHandler = (status): void => {
    // that's our variable for array of messages
    let msgObj: any = [];
    try {
      if (status.validity === AppConstants.VALID) {
        this.validStates(status.value);
      } else {
        this.invalidStates(status.value);
      }
      // if data is not in JSON format
    } catch (e) {
      console.log(e);
    }
  }

  validStates = (status) => {
    // we start initializing states like when app starts
    this._dataStorage.initValidStates();
    this.appStates = this._dataStorage.getStates();
    // depending on response status we do different actions
    switch (status) {
      // if we receive a message status
      case (AppConstants.MESSAGE_RESPONSE):
        // we define our state like user logged in
        this.appStates[AppConstants.MESSAGE_RESPONSE] = true;
        this._dataStorage.setStates(this.appStates);
        // we return data to our main component to let it draw it on view
        break;
      case (AppConstants.UPDATE_MESSAGES):
        this.appStates[AppConstants.UPDATE_MESSAGES] = true;
        this._dataStorage.setStates(this.appStates);
        break;
      // if we receive login status
      case (AppConstants.LOGIN_RESPONSE):
        // we will going to show modal window with log in
        // we set connecting state to true, to show log in window
        this.appStates[AppConstants.CONNECTING] = true;
        this._dataStorage.setStates(this.appStates);
        break;
      case (AppConstants.SEND_COOKIE):
        // we define our state like user logged in
        this.appStates[AppConstants.SEND_COOKIE] = true;
        this._dataStorage.setStates(this.appStates);
        break;
      case (AppConstants.NICK_CHANGED):
      case (AppConstants.USER_LOGGED):
        // we define our state like user logged in
        this.appStates[AppConstants.LOGGED_IN] = true;
        this._dataStorage.initInvalidStates();
        this._dataStorage.setStates(this.appStates);
        break;
      case (AppConstants.CHANGING_NICKNAME):
        // we set changeNick state to true, this will open login window
        this.appStates[AppConstants.CHANGING_NICKNAME] = true;
        this._dataStorage.setStates(this.appStates);
        break;
      case (AppConstants.PRIVATE_MESSAGE):
        this.appStates[AppConstants.PRIVATE_MESSAGE] = true;
        this._dataStorage.setStates(this.appStates);
        break;
      case (AppConstants.UPDATE_TABS):
        // we define our state like user logged in
        this.appStates[AppConstants.UPDATE_TABS] = true;
        this._dataStorage.setStates(this.appStates);
        break;
      case (AppConstants.GENERIC_MODAL):
        // we define our state like user logged in
        this.appStates[AppConstants.GENERIC_MODAL] = true;
        this._dataStorage.setStates(this.appStates);
        break;
      case (AppConstants.SHOW_USERID):
        // we define our state like user logged in
        this.appStates[AppConstants.SHOW_USERID] = true;
        this._dataStorage.setStates(this.appStates);
        break;
      case (AppConstants.LOG_OFF):
        // we define our state like user logged in
        this.appStates[AppConstants.LOG_OFF] = true;
        this._dataStorage.setStates(this.appStates);
        break;
    }
  }

  invalidStates = (status) => {
    // we start initializing states like when app starts
    this._dataStorage.initInvalidStates();
    this.appStates = this._dataStorage.getStates();
    switch (status) {
      case (AppConstants.NICK_UNDERSCORE):
        // we set changeNick state to true, this will open login window
        this.appStates[AppConstants.NICK_UNDERSCORE] = true;
        this._dataStorage.setStates(this.appStates);
        break;
      case (AppConstants.NICK_SAME):
        // we set changeNick state to true, this will open login window
        this.appStates[AppConstants.NICK_SAME] = true;
        this._dataStorage.setStates(this.appStates);
        break;
      case (AppConstants.NICK_IN_USE):
        // we set changeNick state to true, this will open login window
        this.appStates[AppConstants.NICK_IN_USE] = true;
        this._dataStorage.setStates(this.appStates);
        break;
      case (AppConstants.USER_MULTISESSION):
        // we set changeNick state to true, this will open login window
        this.appStates[AppConstants.USER_MULTISESSION] = true;
        this._dataStorage.setStates(this.appStates);
        break;
      // if we receive a closed or error state
      case (AppConstants.ERROR_STATE):
      case (AppConstants.CLOSED_CONNECTION):
        // we set closed state, it will make all components close buttons and show error messages
        this._dataStorage.initValidStates();
        this.appStates[AppConstants.CLOSED_CONNECTION] = true;
        this._dataStorage.setStates(this.appStates);
        // we return status to main component
        break;
      case (AppConstants.BANNED):
        // we set closed state, it will make all components close buttons and show banned message
        this._dataStorage.initValidStates();
        this.appStates[AppConstants.BANNED] = true;
        this._dataStorage.setStates(this.appStates);
        // we return status to main component
        break;
    }
  }
}
