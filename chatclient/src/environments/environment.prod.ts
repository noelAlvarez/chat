export const environment = {
  production: true,
  ws_url: 'ws://leadingchat.herokuapp.com',
  http_url: 'http://leadingchat.herokuapp.com'
};
