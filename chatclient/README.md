
# Deployment Instructions

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.7.

## Development server

First, run `npm install` command, to install all libraries needed for project.

Secod, run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Deployment into another environment

Run `ng build` to build the project. All compiled code will be stored in the `dist/` directory. Deploy dist content to your Tomcat server or any other server. 
Use the `-prod` flag for a production build. If you want an optimized build use ng `build --aot`.
Use environment.prod when you deploying in production environment.
