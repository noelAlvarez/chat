/*jslint node: true, indent: 2 */
'use strict';
var express = require('express');
var util = require('util');
var bodyParser = require("body-parser");
var request = require("request");
var compression = require('compression');

//type of response
var status = "";

var app = express();
app.use(compression());
app.set('name', "Chat");
app.set('port', 8888);

var server = require('http').createServer(app);
app.use(require('morgan')('dev'));
app.use(compression());
var wssServer = require('ws').Server({
    server: server,
    perMessageDeflate: false
});
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/../dist'));

app.use(express.static('../package.json'));

//set cors headers
app.use(function (req, res, next) {
    if (req.hostname == "leadingchat.herokuapp.com") {
        res.header("Access-Control-Allow-Origin", "http://leadingchat.herokuapp.com");
    } else if (req.hostname == "localhost") {
        res.header("Access-Control-Allow-Origin", "http://localhost:4200");
    }
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var security;
var env = new require('./config');
var dbConnection = new require('./dbconnection')(env);
var chats = new require('./chathistory')();
var streaming = new require('./streaming')();
var users;
var chatProtocol;
var dbMsgs;
var dbUsers;
dbConnection.connect().then(() => {
    security = new require('./security')(wssServer, dbConnection);
    security.getBlacklist();
    dbMsgs = new require('./dbqueriesMsgs')(dbConnection, dbUsers);
    dbUsers = new require('./dbqueriesUsers')(dbConnection, dbMsgs);
    users = new require('./users')(chats, dbUsers, security);
    chats = new require('./chathistory')(users, dbMsgs);
    chatProtocol = new require('./protocol')(wssServer, users, chats);

    app.use('*', function (req, res, next) {
        let ip = req.headers['x-forwarded-for'] || req._remoteAddress;
        security.countRequest(ip);
        if (req.body.token != 'cache=1521469751495ja8v2cf5indydosccf8nmldjzcd4ov2o3ctgeum9p7ix6dwfuqw6alt9gfne3xkibxquqs2pz7s2tsadkzqu5u8a5z599zfqmfs271keddgdjejwep41tuebe7ulvh3vwjfwzpelhlz3xmzze744y3u8je7eff25wbuee9pf5s9zf7e0o9r32cd6ksohwp') {
            if (security.isUserBlacklisted(ip)) {
                res.status(200).send({
                    errors: 'banned',
                });
                return false;
            }
        }
        next();
    });
    //REST routes
    app.use(require('./routes')(users, chats, security, chatProtocol).router);

    dbMsgs.deleteOldMsgs();
    dbUsers.deleteOldUsers();
    users.removeConnectionList();
    users.checkConnectedUsers();
    //dbMsgs.drawManyMessages();
}).catch(function (e) {
    console.log('dbConnection:' + e);
});
wssServer.on('connection', function (ws) {
    if (chatProtocol) {
        let obj = {}
        let ip = ws.upgradeReq.headers['x-forwarded-for'] || ws.upgradeReq.client.remoteAddress;
        security.countRequest(ip);
        if (security.isUserBlacklisted(ip)) {
            chatProtocol.sendUserBanned(ws);
        } else {
            chatProtocol.sendValidLogin(ws);
        }
    }
    ws.on('message', function (msg) {
        let ip = ws.upgradeReq.headers['x-forwarded-for'] || ws.upgradeReq.client.remoteAddress;
        //security
        security.countRequest(ip);
        if (security.isUserBlacklisted(ip)) {
            chatProtocol.sendUserBanned(ws);
        } else {
            if (msg[0] === '/') {
                chatProtocol.handleCommand(msg, ws);
            } else {
                if (ws.user && msg) { //we know who this is
                    if (msg.length > 5000) {
                        msg = msg.substr(0, 5000);
                    }
                    chatProtocol.broadcastMessage(ws.user.nick, msg);
                }
            }
        }
    });
    ws.on('close', function (code, message) {
        let user = ws.user;
        if (user) {
            users.setConnectionState(user.nick, false);
            util.log(user.nick + " dropped out.");
            let msg = user.nick + " dropped out.";
            chatProtocol.broadcastMessage("_server", msg);
        }
    });
});
wssServer.on('error', function (err) {
    util.log("Websocket server error: " + util.inspect(err));
});

util.log('Server started.');
server.listen(process.env.PORT || app.get('port'), function () {
    util.log('%s listening at %s', app.get('name'), app.get('port'));
});

var http = require("http");
// setInterval(function () {
//     http.get("http://leadingchat.herokuapp.com");
// }, 120000);