var PeerServer = require('peer').PeerServer;
exports = module.exports = function () {
    var server = new PeerServer({
        port: 9000,
        path: '/peerConn'
    });
    var connected = [];
    server.on('connection', function (id, user) {
        console.log('connected: ' + id);
        // var idx = connected.indexOf(id); // only add id if it's not in the list yet
        // if (idx === -1) {
        //     connected.push(id);
        // }
    });
    server.on('disconnect', function (id) {
        console.log('disconnected: ' + id);
        // var idx = connected.indexOf(id); // only attempt to remove id if it's in the list
        // if (idx !== -1) {
        //     connected.splice(idx, 1);
        // }
    });
}