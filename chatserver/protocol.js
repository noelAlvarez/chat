exports = module.exports = function (_wss, users, _chathistory) {
    var _users = users;
    let ip = '';
    return {
        handleCommand: function (msg, ws) {
            let commandAndParmas = msg.split(" ");
            var index = msg.indexOf(' ', msg.indexOf(' ') + 1);
            let msgString = msg.substring(index + 1);
            this.setClient(ws);
            let nick = '';
            ip = ws.upgradeReq.headers['x-forwarded-for'] || ws.upgradeReq.client.remoteAddress;
            if (commandAndParmas[0].toLowerCase() === '/nick') {
                nick = commandAndParmas[1].trim();
                if (nick[0] === '_') {
                    this.sendUnderscore();
                    return false;
                }
                // if user exist we call change nick function
                if (ws.user) {
                    // we take actual nick
                    var oldNick = ws.user.nick;
                    this.changeNick(oldNick, nick, ws.user.token);

                } else {
                    this.createNewUser(nick);
                }
                // private messages has this format: /privateMsg toUser message
            } else if (commandAndParmas[0].toLowerCase() === '/login') {
                let token = commandAndParmas[1].trim();
                _users.getUserByToken(token).then((user) => {
                    if (user) {
                        this.loginExistingUser(user);
                    }
                }).catch(function (e) {
                    console.log('protocol autologin getUserByToken call:' + e);
                });
            } else if (commandAndParmas[0].toLowerCase() === '/query') {
                let toUser = commandAndParmas[1].trim();
                let wsToUser = this.getWsUser(toUser);
                if (wsToUser && msgString != "" && msgString.indexOf("cache=") == -1) {
                    this.addToHistory(ws.user.token, commandAndParmas[1].trim(), msgString).then((dataObj) => {
                        let responseObj = _chathistory.mapResponseToUser(ws.user.token, ws.user.nick, commandAndParmas[1].trim(), ws.user.token, dataObj.toUser, dataObj);
                        this.sendPrivate(responseObj, ws.user.nick, commandAndParmas[1].trim(), wsToUser);
                    }).catch(function (e) {
                        console.log('protocol privates addtohistory:' + e);
                    });
                }
            } else if (commandAndParmas[0].toLowerCase() === '/cam' || commandAndParmas[0].toLowerCase() === '/call') {
                let userFromToken = commandAndParmas[2].trim();
                users.getUserByToken(userFromToken).then((userFrom) => {
                    if (userFrom) {
                        let wsToUser = this.getWsUser(commandAndParmas[1].trim());
                        if (wsToUser) {
                            let obj = {};
                            let userTo = commandAndParmas[1].trim();
                            let msgString = 'User ' + userFrom.nick + ', wants to webCam you, did you accept?';
                            obj['streamQuery'] = true;
                            obj['clientID'] = commandAndParmas[3].trim();
                            obj['streamType'] = commandAndParmas[0].toLowerCase().substring(1);
                            this.addToHistory(userFromToken, userTo, msgString, obj).then((dataObj) => {
                                let responseObj = _chathistory.mapResponseToUser(userFromToken, userFrom.nick, userTo, userFromToken, dataObj.toUser, dataObj, true);
                                this.checkUserTo(userFrom.nick, userTo, responseObj, wsToUser);
                            }).catch(function (e) {
                                console.log('protocol privates addtohistory:' + e);
                            });
                        }
                    }
                });
            }
        },
        broadcast: function (from, msg, status) {
            if (msg != "" && msg.indexOf("cache=") == -1) {
                this.addToHistory(from, null, msg).then((dataObj) => {
                    let chat = {
                        status: status,
                        msg: dataObj
                    };
                    for (let i = 0; i < _wss.clients.length; i++) {
                        let client = _wss.clients[i];
                        if (client.user) {
                            if (from === "_server") {
                                client.send(JSON.stringify(chat));
                            } else {
                                _users.getIgnored(client.user.nick, [from], client.user.token).then((result) => {
                                    result[0] ? null : client.send(JSON.stringify(chat));
                                })
                            }
                        }
                    };
                }).catch(function (e) {
                    console.log('procotol broadcast addtohistory call:' + e);
                });
            }
        },
        addToHistory: function (from, toUser, msg, auxData) {
            return new Promise((resolve, reject) => {
                this.msgObjMap(from, toUser, msg, auxData).then((responseObj) => {
                    _chathistory.add(responseObj);
                    resolve(responseObj);
                })
            });
        },
        msgObjMap: function (from, toUser, msg, auxData) {
            return new Promise((resolve, reject) => {
                let msgObj = {
                    from: from,
                    timestamp: Date.now()
                }
                if (msg.indexOf('?imgFile') != -1) {
                    msgObj['image'] = msg;
                } else if (msg.indexOf('?audioFile') != -1) {
                    msgObj['audio'] = msg;
                } else if (msg.indexOf('?videoFile') != -1) {
                    msgObj['video'] = msg;
                } else {
                    msgObj['msg'] = msg;
                }
                if (auxData) {
                    Object.assign(msgObj, auxData);
                }
                if (toUser) {
                    _users.getUserToken(toUser).then((toUserToken) => {
                        msgObj['toUser'] = toUserToken;
                        resolve(msgObj);
                    }).catch(function (e) {
                        console.log('protocol msgObjMap:' + e);
                    });
                } else {
                    resolve(msgObj);
                }
            });
        },
        getWsUser: function (user) {
            let wsUser = null;
            for (let i = 0; i < _wss.clients.length; i++) {
                let client = _wss.clients[i];
                if (client.user && client.user.nick === user) {
                    wsUser = client;
                    break;
                }
            };
            return wsUser;
        },
        createNewUser: function (userName) {
            var that = this;
            _users.createUser(userName, ip).then((responseToCreateUser) => {
                if (responseToCreateUser == "Nick in use") {
                    this.sendNickInUse();
                    return false;
                } else if (responseToCreateUser == "multisession") {
                    this.sendMultisession();
                    return false;
                } else {
                    this.saveUser(responseToCreateUser);
                    this.sendUserLogged();
                    this.sendUserToken(responseToCreateUser.token);
                    this.broadcastMessage("_server", "New user " + userName + " joined.");
                }
            }).catch(function (e) {
                console.log('protocol createnewuser createuser call:' + e);
            });
        },
        loginExistingUser: function (user) {
            let obj = {};
            _users.setConnectionState(user.nick, true).then(() => {
                this.sendUserLogged();
                this.saveUser(user);
                this.broadcastMessage("_server", "User " + user.nick + " joined.");
            }).catch(function (e) {
                console.log('procotol loginexistinguser setconnectionstate call:' + e);
            });
        },
        changeNick: function (oldNick, newNick, token) {
            _users.changeNick(oldNick, newNick, token).then((responseNickChange) => {
                if (responseNickChange == "Nick in use") {
                    this.sendNickInUse();
                    return false;
                } else if (responseNickChange == "that's your actual nick") {
                    this.sendNickSame();
                    return false;
                } else {
                    this.sendNickChanged();
                    this.broadcastMessage("_server", oldNick + " is now known as " + newNick);
                    this.broadcastUpdateTabs(oldNick, newNick);
                    let wsClient = this.getClient();
                    wsClient.user.nick = newNick;
                    this.saveUser(wsClient.user);
                }
            }).catch(function (e) {
                console.log('protocol changenick:' + e);
            });
        },
        sendPrivate: function (responseObj, userFrom, userTo, wsToUser) {
            let obj = {};
            obj['status'] = {
                validity: "valid",
                value: "privateMessage",
            };
            obj['msg'] = responseObj;
            obj['fromUser'] = userFrom;
            obj['toUser'] = userTo;
            let ws = this.getClient();
            ws.send(JSON.stringify(obj));
            this.checkUserTo(userFrom, userTo, responseObj, wsToUser);
        },
        checkUserTo: function (userFrom, userTo, responseObj, wsToUser) {
            _users.getIgnored(userTo, [userFrom], null, true).then((userIgnored) => {
                if (!userIgnored) {
                    let obj = {};
                    obj['status'] = {
                        validity: "valid",
                        value: "privateMessage",
                    };
                    obj['msg'] = responseObj;
                    obj['fromUser'] = userFrom;
                    obj['toUser'] = userTo;
                    responseObj.mine = !responseObj.mine;
                    obj['msg'] = responseObj;
                    wsToUser.send(JSON.stringify(obj));
                }
            }).catch(function (e) {
                console.log('protocol checkuserTo:' + e);
            });
        },
        sendNickSame: function () {
            let obj = {};
            let ws = this.getClient();
            obj['status'] = {
                validity: "invalid",
                value: "nickSame"
            };
            ws.send(JSON.stringify(obj));
        },
        sendNickChanged: function () {
            let obj = {};
            let ws = this.getClient();
            obj['status'] = {
                validity: "valid",
                value: "nickChanged"
            };
            ws.send(JSON.stringify(obj));
        },
        sendUnderScore: function () {
            let obj = {};
            let ws = this.getClient();
            //reserver nicks starting with '_' to server use
            obj['status'] = {
                validity: "invalid",
                value: "underscore"
            };
            ws.send(JSON.stringify(obj));
        },
        sendValidLogin: function (ws) {
            let obj = {};
            let wsUser = ws || this.getClient();
            obj['status'] = {
                validity: "valid",
                value: "login"
            };
            wsUser.send(JSON.stringify(obj));
        },
        sendUserToken: function (cookie) {
            let obj = {};
            let ws = this.getClient();
            let expire = this.getExpireTime();
            if (ws) {
                obj['status'] = {
                    validity: "valid",
                    value: "cookie",
                    cookie: cookie + expire
                };
                ws.send(JSON.stringify(obj));
            }
        },
        sendMultisession: function () {
            let obj = {};
            let ws = this.getClient();
            obj['status'] = {
                validity: "invalid",
                value: "multiSession"
            };
            ws.send(JSON.stringify(obj));
        },
        sendUserLogged: function () {
            let obj = {};
            let ws = this.getClient();
            obj['status'] = {
                validity: "valid",
                value: "userLogged"
            };
            ws.send(JSON.stringify(obj));
        },
        sendNickInUse: function () {
            let obj = {};
            let ws = this.getClient();
            obj['status'] = {
                validity: "invalid",
                value: "nickInUse"
            };
            ws.send(JSON.stringify(obj));
        },
        sendServerOnMaintenance: function (ws) {
            let obj = {};
            let wsUser = ws || this.getClient();
            obj['status'] = {
                validity: "invalid",
                value: "serverOnMaintenance"
            };
            wsUser.send(JSON.stringify(obj));
        },
        sendUserBanned: function (ws) {
            let obj = {};
            let wsUser = ws || this.getClient();
            obj['status'] = {
                validity: "invalid",
                value: "banned"
            };
            wsUser.send(JSON.stringify(obj));
        },
        broadcastMessage: function (user, msg) {
            let status = {
                validity: "valid",
                value: "message"
            };
            this.broadcast(user, msg, status);
        },
        broadcastUpdateTabs: function (oldNick, newNick) {
            let status = {
                validity: "valid",
                value: "updateTabs",
                oldNick: oldNick,
                newNick: newNick
            };
            this.broadcast("_server", null, status);
        },
        saveUser: function (user) {
            let ws = this.getClient();
            ws.user = user;
        },
        setClient: function (client) {
            this.client = client;
        },
        getClient: function () {
            return this.client;
        },
        getExpireTime: function () {
            var now = new Date();
            var time = now.getTime();
            time += 3600 * 300000000;
            now.setTime(time);
            return ';expires=' + now.toUTCString();
        },
        closeServer: function (oldNick, newNick) {
            _wss.close();
        }
    }
};