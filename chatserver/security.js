// that's our model for blacklist
function Blacklist(ip) {
    this.ip = ip;
    this.age = Date.now()
}

// that's our model for requests count
function CountList(ip) {
    this.ip = ip;
    this.tries = 1;
}

exports = module.exports = function (_ws, dbConnection) {
    var _countList = {};
    var _blacklist;
    let dbBlacklist = dbConnection.getDbBlacklist();
    //remover of users in count list that not reach blacklist status
    setInterval(() => {
        Object.getOwnPropertyNames(_countList).forEach(function (val, idx, array) {
            // if user makes 20 request in 10 seconds we add it to blacklist
            if (_countList[val].tries >= 80) {
                dbBlacklist.collection('_blacklist').insertOne(new Blacklist(val), (err, res) => {
                    if (err) {
                        console.log('insert blacklist');
                        console.error(err);
                    }
                });
                _blacklist[val] = new Blacklist(val);
            }
            delete _countList[val];
        });
    }, 10000);
    //remover of users in blacklist when fulfill punishment
    setInterval(() => {
        // if user stays one day in black list we remove it (45000000)
        dbBlacklist.collection('_blacklist').remove({
                'age': {
                    $lte: (Date.now() - 90000000)
                },
            },
            true,
            (err, res) => {
                if (err) {
                    console.log('delete user in blacklist');
                    console.error(err);
                }
            }
        )
        this.getBlacklist();
    }, 45000000);
    return {
        // check if user is blacklisted by ip
        isUserBlacklisted: function (ip) {
            if (_blacklist && _blacklist[ip]) {
                return true;
            } else {
                return false;
            }
        },
        // add one request to request counter for this ip
        countRequest: function (ip) {
            if (_countList[ip]) {
                _countList[ip].tries++;
            } else {
                _countList[ip] = new CountList(ip);
            }
            console.log(ip + ' intentos:' + _countList[ip].tries++);
        },
        getBlacklist: function () {
            return new Promise((resolve, reject) => {
                dbBlacklist.collection('_blacklist').find({}).toArray((err, list) => {
                    if (err) {
                        console.log('get blacklist');
                        console.error(err);
                        reject(err);
                    }
                    _blacklist = {}
                    if (list.length > 0) {
                        for (let i = 0; i < list.length; i++) {
                            _blacklist[list[i].ip] = list[i];
                        }
                    }
                });
            });
        },
        getToken: function () {
            return new Promise((resolve, reject) => {
                let token = this.createToken();
                let cookie = 'cache=' + token;
                resolve(cookie);
            });
        },
        createToken: function () {
            let tknContent = '';
            let tknContent1 = Date.now().toString();
            let tknContent2 = this.getRandomString(8);
            return tknContent1 + tknContent2;
        },
        getRandomString: function (length) {
            let string = '';
            for (i = 0; i <= length; i++) {
                string = string + Math.pow(Math.random(), Math.random()).toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
            }
            return string;
        },
        insertBlackList: function (ip) {
            dbBlacklist.collection('_blacklist').insertOne(new Blacklist(ip), (err, res) => {
                if (err) {
                    console.log('insert blacklist');
                    console.error(err);
                }
            });
            _blacklist[ip] = new Blacklist(ip);
        },
        deleteBlackList: function (ip) {
            // if user stays one day in black list we remove it (45000000)
            dbBlacklist.collection('_blacklist').remove({
                    'ip': ip
                },
                true,
                (err, res) => {
                    if (err) {
                        console.log('delete user in blacklist');
                        console.error(err);
                    }
                }
            )
            delete _blacklist[ip];
        }
    }
}