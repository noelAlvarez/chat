exports = module.exports = function (dbConnection) { //000000
    //this.deleteMessagesByTime('_chatsMain', 1520857323095);
    let dbMsgs = dbConnection.getDbMsgs();
    let dbMsgsPrivate = dbConnection.getDbMsgsPrivate();
    //let dbHistoric = dbConnection.getDbMsgsHistoric();
    return {
        getMessages: function (table, range, ignoredList) {
            return new Promise((resolve, reject) => {
                dbMsgs.collection(table).find({
                    'from': {
                        $nin: ignoredList
                    }
                }).sort({
                    $natural: -1
                }).limit(range).toArray((err, msgs) => {
                    if (err) {
                        console.log('getMessages');
                        console.error(err);
                        reject(err);
                    }
                    if (msgs) {
                        if (msgs.length > 1) {
                            this.getDatabaseLength(table).then((dbLength) => {
                                resolve({
                                    msgs: msgs.reverse(),
                                    dbLength: dbLength
                                });
                            }).catch(function (e) {
                                console.log('getMessages getDbLength call:' + e);
                            });
                        } else {
                            resolve({
                                msgs: msgs,
                                dbLength: 0
                            });
                        }
                    }
                })
            })
        },
        getPrivateMessages: function (table, range, fromToken, toToken) {
            return new Promise((resolve, reject) => {
                dbMsgsPrivate.collection(table).find({
                    $or: [{
                            from: fromToken,
                            toUser: toToken
                        },
                        {
                            from: toToken,
                            toUser: fromToken
                        }
                    ]
                }).sort({
                    timestamp: -1
                }).limit(range).toArray((err, msgs) => {
                    if (err) {
                        console.log('getPrivateMessages');
                        console.error(err);
                        reject(err);
                    } else {
                        if (msgs) {
                            if (msgs.length > 1) {
                                this.getDatabaseLength(table).then((dbLength) => {
                                    resolve({
                                        msgs: msgs.reverse(),
                                        dbLength: dbLength
                                    });
                                }).catch(function (e) {
                                    console.log('getPrivateMessages getDbLength call:' + e);
                                });
                            } else {
                                resolve({
                                    msgs: msgs,
                                    dbLength: 1
                                });
                            }
                        } else {
                            resolve(null);
                        }
                    }
                })
            })
        },
        getLastMessage: function (table) {
            return new Promise((resolve, reject) => {
                dbMsgs.collection(table).find({}).sort({
                    timestamp: -1
                }).limit(1).toArray((err, msgs) => {
                    if (err) {
                        console.log('getLastMessage');
                        console.error(err);
                        reject(err);
                    } else {
                        resolve(msgs[0]);
                    }
                })
            })
        },
        getLastPrivateMessage: function (table, fromToken, toToken) {
            return new Promise((resolve, reject) => {
                dbMsgsPrivate.collection(table).find({
                    $or: [{
                            from: fromToken,
                            toUser: toToken
                        },
                        {
                            from: toToken,
                            toUser: fromToken
                        }
                    ]
                }).sort({
                    timestamp: -1
                }).limit(1).toArray((err, msgs) => {
                    if (err) {
                        console.log('getLastPrivate');
                        console.error(err);
                        reject(err);
                    } else {
                        resolve(msgs);
                    }
                })
            })
        },
        insert: function (dataObj, table) {
            return new Promise((resolve, reject) => {
                let chooseDb;
                if (table == "_chatsMain") {
                    chooseDb = dbMsgs;
                } else {
                    chooseDb = dbMsgsPrivate;
                }
                chooseDb.collection(table).insertOne(dataObj, (err, res) => {
                    if (err) {
                        console.log('insert');
                        console.error(err);
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
        },
        insertHistoric: function (table, dataObj) {
            this.connectionHistoric().then(() => {
                dbHistoric.collection(table).insert(dataObj, (err, res) => {
                    if (err) {
                        console.log('insert historic');
                        console.error(err);
                    }
                });
            })
        },
        getDatabaseLength: function (table) {
            return new Promise((resolve, reject) => {
                let chooseDb;
                if (table == "_chatsMain") {
                    chooseDb = dbMsgs;
                } else {
                    chooseDb = dbMsgsPrivate;
                }
                chooseDb.collection(table).count((err, res) => {
                    if (err) {
                        console.log('getDBLength');
                        console.error(err);
                        reject(err);
                    } else {
                        resolve(res);
                    }
                })
            });
        },
        replaceNick: function (table, oldNick, newNick) {
            dbMsgs.collection(table).update({
                'from': oldNick
            }, {
                $set: {
                    'from': newNick
                }
            }, {
                multi: true
            });
        },
        declinedStream: function (streamID) {
            dbMsgsPrivate.collection('_chatsPrivate').update({
                'id': streamID
            }, {
                $set: {
                    'expired': true
                }
            });
        },
        getMessagesByTime: function (table, time) {
            return new Promise((resolve, reject) => {

                dbMsgs.collection(table).find({
                    timestamp: {
                        $lte: time
                    },
                }).toArray((err, msgs) => {
                    if (err) {
                        console.log('getMsgByTime');
                        console.error(err);
                        reject(err);
                    } else {
                        resolve(msgs);
                    }
                })
            })
        },
        deleteMessagesByTime: function (table, time) {
            dbMsgs.collection(table).remove({
                    timestamp: {
                        $lte: time
                    },
                },
                true,
                (err, res) => {
                    if (err) {
                        console.log('deleteMsgByTime');
                        console.error(err);
                    }
                }
            )
        },
        deleteMessagesByRange: function (table, range) {
            dbMsgs.collection(table).find({}).skip(range - 1).limit(1).toArray(function (err, msgObj) {
                if (err) {
                    console.log('findRange');
                    console.error(err)
                }
                dbMsgs.collection(table).remove({
                        timestamp: {
                            $lte: msgObj[0].timestamp
                        },
                    },
                    true,
                    (err, res) => {
                        if (err) {
                            console.log('deleteMsgByRange');
                            console.error(err);
                        }
                    }
                )
            })
        },
        //remove old main messages every 12h (43200000)
        deleteOldMsgs: function () {
            var that = this;
            setInterval(() => {
                that.getDatabaseLength('_chatsMain').then((dbLength) => {
                    if (dbLength > 40000) {
                        //this.insertHistoric('_chatsMain', msgs);
                        that.deleteMessagesByRange('_chatsMain', dbLength - 30000);
                    }
                }).catch(function (e) {
                    console.log('deleteOldMsgs getDbLength call:' + e);
                });
            }, 43200000);
        },
        deletePrivateMessages: function (userToken) {
            return new Promise((resolve, reject) => {
                dbMsgsPrivate.collection('_chatsPrivate').remove({
                        $or: [{
                                from: userToken,
                            },
                            {
                                toUser: userToken
                            }
                        ]
                    }, true,
                    (err, res) => {
                        if (err) {
                            console.log('deleteMsgByRange');
                            console.error(err);
                        }
                        resolve();
                    }
                )
            })
        },
        drawManyMessages: function () {
            let r = new Array;
            let msg = '';
            let objs = [];
            for (let i = 0; i < 100000; i++) {
                let obj = {};
                obj.from = i.toString();
                obj.msg = 'msgfefvodvnhodivoisdvnoivniosdfvnsdiovdnsñvoisnvodivbnovuibndiovbvuf' + i;
                obj.timestamp = Date.now();
                objs.push(obj);
            }
            this.connection().then(() => {
                dbMsgs.collection('_chatsMain').insert(objs, function (err, res) {
                    if (err) throw err;
                });
            });
        },
    }
}