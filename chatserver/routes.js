 var express = require('express');

 exports = module.exports = function (_users, _chathistory, _security, chatProtocol) {
     var router = express.Router();

     router.route('/users').get(function (req, res) {
         _users.getAll().then((users) => {
             res.status(200).send({
                 errors: null,
                 data: users
             });
         }).catch(function (e) {
             console.log('route getAllUsers:' + e);
         });
     });

     router.route('/history').post(function (req, res) {
         _chathistory.getAll(req.body.index, req.body.ignoreds).then((msgs) => {
             res.status(200).send({
                 errors: null,
                 data: msgs
             });
         }).catch(function (e) {
             console.log('router getAllMsgs:' + e);
         });
     });

     router.route('/historyPrivate').post(function (req, res) {
         _chathistory.getPrivates(req.body.fromUser, req.body.toUser, req.body.cache, req.body.index).then((msgObj) => {
             res.status(200).send({
                 errors: null,
                 data: msgObj
             });
         }).catch(function (e) {
             console.log('router getAllPrivates:' + e);
         });
     });

     router.route('/lastMsg').get(function (req, res) {
         _chathistory.getLastMessage().then((msg) => {
             res.status(200).send({
                 errors: null,
                 data: msg
             });
         }).catch(function (e) {
             console.log('router lastMsg:' + e);
         });
     });

     router.route('/lastPrivateMsg').post(function (req, res) {
         _chathistory.getLastPrivate(req.body.fromUser, req.body.toUser, req.body.cache).then((msg) => {
             res.status(200).send({
                 errors: null,
                 data: msg
             });
         }).catch(function (e) {
             console.log('router LastPrivateMsg:' + e);
         });
     });

     router.route('/ignoreUser').post(function (req, res) {
         res.status(200).send({
             errors: null,
             data: _users.setIgnored(req.body.cache, req.body.ignoredUser)
         });
     });

     router.route('/getIgnoreds').post(function (req, res) {
         _users.getIgnored(req.body.user, req.body.userList, req.body.cache, false).then((result) => {
             res.status(200).send({
                 errors: null,
                 data: result
             });
         })
     });

     router.route('/getToken').get(function (req, res) {
         _security.getToken().then((token) => {
             res.status(200).send({
                 errors: null,
                 data: token
             });
         });
     });

     router.route('/checkCookie').post(function (req, res) {
         let ip = req.headers['x-forwarded-for'] || req._remoteAddress;
         _users.checkCookie(req.body.cookie, ip).then((response) => {
             if (response) {
                 let result = response[0];
                 if (result) {
                     if (typeof result == 'object') {
                         res.status(200).send({
                             errors: null,
                             data: result
                         });
                     } else if (result == 'multisession') {
                         res.status(200).send({
                             errors: null,
                             data: "multisession"
                         });
                     } else {
                         res.status(200).send({
                             errors: null,
                             data: false
                         });
                     }
                 } else {
                     res.status(200).send({
                         errors: null,
                         data: false
                     });
                 }
             } else {
                 res.status(200).send({
                     errors: null,
                     data: false
                 });
             }
         }).catch(function (e) {
             console.log('router checkcookie:' + e);
         });
     });

     router.route('/checkConnectionState').post(function (req, res) {
         _users.confirmConnectionState(req.body.nick);
         res.status(200).send({
             errors: null,
             data: true
         });
     });

     router.route('/declinedStream').post(function (req, res) {
         _chathistory.declinedStream(req.body.streamID);
         res.status(200).send({
             errors: null,
             data: true
         });
     });

     router.route('/abcdefghijk_0123').post(function (req, res) {
         if (req.body.token == 'cache=1521469751495ja8v2cf5indydosccf8nmldjzcd4ov2o3ctgeum9p7ix6dwfuqw6alt9gfne3xkibxquqs2pz7s2tsadkzqu5u8a5z599zfqmfs271keddgdjejwep41tuebe7ulvh3vwjfwzpelhlz3xmzze744y3u8je7eff25wbuee9pf5s9zf7e0o9r32cd6ksohwp') {
             if (req.body.close) {
                 chatProtocol.closeServer();
             } else if (req.body.updateBlackList) {
                 _security.getBlacklist();
             } else if (req.body.blackList) {
                 _security.insertBlackList(req.body.blackList);
             } else if (req.body.deleteblackList) {
                 _security.deleteBlackList(req.body.deleteblackList);
             }
         }
         res.status(200).send({
             errors: null,
             data: true
         });
     });

     return {
         router: router
     }
 };