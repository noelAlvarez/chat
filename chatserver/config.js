const env = process.env.NODE_ENV || 'dev'; // 'dev' or 'production'

const dev = {
    db: {
        users: {
            connectionString: 'mongodb://127.0.0.1:27017/dbUsers',
            name: 'dbUsers'
        },
        msgs: {
            connectionString: 'mongodb://127.0.0.1:27017/dbMsgs',
            name: 'dbMsgs',
        },
        privates: {
            connectionString: 'mongodb://127.0.0.1:27017/dbPrivates',
            name: 'dbPrivates',
        },
        blacklist: {
            connectionString: 'mongodb://127.0.0.1:27017/dbBlacklist',
            name: 'dbBlacklist',
        }
    }
};

const prod = {
    db: {
        users: {
            connectionString: 'mongodb://mydbuser:skolastic0@ds119449.mlab.com:19449/db_chat_mongo3',
            name: 'db_chat_mongo3'
        },
        msgs: {
            connectionString: 'mongodb://mydbuser:skolastic0@ds211309.mlab.com:11309/db_chat_mongo',
            name: 'db_chat_mongo'
        },
        privates: {
            connectionString: 'mongodb://mydbuser:skolastic0@ds113849.mlab.com:13849/db_chat_mongo2',
            name: 'db_chat_mongo2'
        },
        blacklist: {
            connectionString: 'mongodb://mydbuser:skolastic0@ds135089.mlab.com:35089/db_chat_mongo4',
            name: 'db_chat_mongo4'
        }
    }
};

if (env.indexOf('production') == 0) {
    module.exports = prod;
} else {
    module.exports = dev;
};