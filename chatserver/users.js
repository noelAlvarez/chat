var _ = require('lodash');

// that our model of public user list to give to our application
function User(nick, ip, state, token, ignoredUsers) {
    this.nick = nick;
    this.ip = ip;
    this.online = state;
    this.token = token;
    this.ignoredUsers = ignoredUsers;
    this.lastConnection = null;
}

exports = module.exports = function (chats, _db, _security) {
    var _users = {};
    var _usersPrivate = {};
    var _connectionList = [];
    return {
        createUser: function (nick, ip) {
            var that = this;
            return new Promise((resolve, reject) => {
                _security.getToken().then((cookie) => {
                    // check if user has more than one session opened in browser
                    that.checkMultisessionAndUser(ip, cookie).then((response) => {
                        let result = response[0];
                        let _users = response[1];
                        // if user with that nick exists
                        if (_users) {
                            for (let i = 0; i < _users.length; i++) {
                                if (_users[i].nick == nick) {
                                    resolve("Nick in use");
                                    return;
                                }
                            }
                        }
                        let _user = {};
                        _user[nick] = new User(
                            nick,
                            ip,
                            true,
                            cookie, [],
                        );
                        _db.insertUser(_user[nick]);
                        resolve(_user[nick]);
                        console.log(_users);
                    }).catch(function (e) {
                        console.log('users createUser checkmulti call:' + e);
                    });
                }).catch(function (e) {
                    console.log('users createUser getToken call:' + e);
                });
            })
        },
        setConnectionState: function (nick, state) {
            return new Promise((resolve, reject) => {
                resolve(_db.setConnectionState(nick, state));
            });
        },
        getUser: function (userName) {
            return new Promise((resolve, reject) => {
                _db.getUser(userName).then((user) => {
                    if (user) {
                        resolve(user);
                    } else {
                        resolve(null);
                    }
                }).catch(function (e) {
                    console.log('user getUser:' + e);
                });
            });
        },
        getUserToken: function (userName) {
            return new Promise((resolve, reject) => {
                this.getUser(userName).then((user) => {
                    if (user) {
                        resolve(user.token);
                    } else {
                        resolve(null);
                    }
                }).catch(function (e) {
                    console.log('users getToken getUser call:' + e);
                });
            })
        },
        getUserByToken: function (token) {
            return new Promise((resolve, reject) => {
                _db.getUserByToken(token).then((user) => {
                    if (user) {
                        resolve(user);
                    } else {
                        resolve(null);
                    }
                }).catch(function (e) {
                    console.log('users getToken getUser call:' + e);
                });
            })
        },
        getAll: function () {
            return new Promise((resolve, reject) => {
                _db.getUsers().then((_users) => {
                    resolve(_.map(_users, function (user) {
                        return {
                            nick: user.nick,
                            online: user.online,
                            ignored: false
                        };
                    }));
                }).catch(function (e) {
                    console.log('users getAll getUsers call:' + e);
                });
            });
        },
        // Changes user nickname
        changeNick: function (oldNick, newNick, token) {
            return new Promise((resolve, reject) => {
                _db.getUsers().then((_users) => {
                    this.checkUser(_users, newNick.toLowerCase(), oldNick.toLowerCase(), token).then((checkResult) => {
                        if (typeof checkResult == 'string') {
                            resolve(checkResult);
                            return;
                        }
                        this.updateUser(newNick, oldNick);
                        resolve();
                    }).catch(function (e) {
                        console.log('users changenick checkuser call:' + e);
                    });
                }).catch(function (e) {
                    console.log('users changenick getUsers call:' + e);
                });
            })
        },
        checkUser: function (_users, newNick, oldNick, token) {
            return new Promise((resolve, reject) => {
                var newUser = {};
                var savedUser;
                for (let i = 0; i < _users.length; i++) {
                    if (_users[i].nick == newNick) {
                        newUser = _users[i];
                    }
                    if (_users[i].nick == oldNick) {
                        savedUser = _users[i];
                    }
                }
                // if new nick exists in user list
                if (newUser.nick) {
                    // if old and new nick are different
                    if (newUser.nick != savedUser.nick) {
                        var savedUserToken = token;
                        var newUserToken = newUser.token;
                        // if new ip and old ip are different, nick is from other user
                        if (savedUserToken != newUserToken) {
                            resolve("Nick in use");
                        }
                        // if new and old nick are same, we didn't change nothing
                    } else {
                        resolve("that's your actual nick");
                    }
                    resolve(true);
                } else {
                    resolve(true);
                }
            });
        },
        updateUser: function (newNick, oldNick) {
            _db.updateUser(oldNick, newNick);
            //this.updateIgnoredNames(oldNick, newNick);
            //_db.replaceNick('_chatsMain', oldNick, newNick);
        },
        checkMultisessionAndUser: function (ip, token) {
            var that = this;
            return new Promise((resolve, reject) => {
                var userExist = null;
                var count = 0;
                var usersLastConnections = [];
                var multisession = null;
                var usersNames = [];
                _db.getUsers().then((_users) => {
                    if (_users.length > 0) {
                        for (let i = 0; i < _users.length; i++) {
                            let user = _users[i];
                            if (user.ip == ip) {
                                count++;
                                usersLastConnections.push(user.lastConnection);
                                usersNames.push(user.nick);
                                if (count >= 12) {
                                    that.deleteOldestUser(usersLastConnections, usersNames);
                                }
                            }
                            if (token && user.token == token) {
                                if (!user.online) {
                                    userExist = user;
                                    break;
                                } else {
                                    multisession = "multisession";
                                    break;
                                }
                            }
                        }
                    }
                    if (multisession) {
                        resolve([multisession, null]);
                    } else if (userExist) {
                        resolve([userExist, _users]);
                    } else {
                        resolve([true, _users]);
                    }
                }).catch(function (e) {
                    console.log('users checkMulti getUsers call:' + e);
                });
            });
        },
        checkCookie: function (token, ip) {
            return new Promise((resolve, reject) => {
                if (token) {
                    this.checkMultisessionAndUser(ip, token).then((response) => {
                        resolve(response);
                    }).catch(function (e) {
                        console.log('users checkcookie checkmulti call:' + e);
                    });
                } else {
                    resolve(null);
                }
            });
        },
        deleteOldestUser: function (usersLastConnections, usersNames) {
            var oldestUser = 0;
            for (var i = 1; i < usersLastConnections.length; i++) {
                if (usersLastConnections[i] < usersLastConnections[oldestUser]) {
                    oldestUser = i;
                }
            }
            _db.deleteUser(usersNames[oldestUser]);
        },
        setIgnored: function (token, ignoredUser) {
            _db.manageIgnored(token, ignoredUser);
        },
        getIgnored: function (userName, ignoredUser, token, security) {
            return new Promise((resolve, reject) => {
                _db.getUsers().then((_users) => {
                    if (_users.length > 0) {
                        var _user;
                        for (let i = 0; i < _users.length; i++) {
                            if (_users[i].nick == userName && _users[i].ignoredUsers.length > 0 && (security || _users[i].token == token)) {
                                _user = _users[i];
                                break;
                            }
                        }
                        if (_user) {
                            let ignoredResult = [];
                            for (let i = 0; i < ignoredUser.length; i++) {
                                _user.ignoredUsers.indexOf(ignoredUser[i]) != -1 ?
                                    ignoredResult.push(true) : ignoredResult.push(false);
                            }
                            resolve(ignoredResult);
                        } else {
                            resolve(false);
                        }
                    }
                }).catch(function (e) {
                    console.log('users getIgnoreds:' + e);
                });
            })
        },
        updateIgnoredNames: function (oldNick, newNick) {
            _db.getUsers().then((_users) => {
                if (_users.length > 0) {
                    let ignoredList = {};
                    for (let i = 0; i < _users.length; i++) {
                        let user = _users[i];
                        let position = user.ignoredUsers.indexOf(oldNick);
                        if (position != -1) {
                            user.ignoredUsers[position] = newNick;
                        }
                    }
                    _db.updateUsers(_users);
                }
            }).catch(function (e) {
                console.log('users updateIgnoreds:' + e);
            });
        },
        confirmConnectionState: function (userName) {
            if (userName) {
                if (_connectionList.indexOf(userName) == -1) {
                    _connectionList.push(userName);
                }
                console.log('agregando: ' + _connectionList);
            }
        },
        removeConnectionList: function () {
            //check if offline users has online state every 10 hours (36000000)
            setInterval(() => {
                _connectionList = [];
            }, 180000);
            console.log('borrando lista');
        },
        checkConnectedUsers: function () {
            //check if offline users has online state every 10 hours (36000000)
            setInterval(() => {
                _db.getOnlineUsers().then((_users) => {
                    if (_users) {
                        for (let i = 0; i < _users.length; i++) {
                            if (_connectionList.indexOf(_users[i].nick) == -1) {
                                this.setConnectionState(_users[i].nick, false);
                                console.log('desconecto a: ' + _users[i].nick);
                            }
                        }
                    }
                });
            }, 480000);
        }
    }
}