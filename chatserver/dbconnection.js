const MongoClient = require('mongodb').MongoClient;
exports = module.exports = function (env) { //000000
    //this.deleteMessagesByTime('_chatsMain', 1520857323095);
    let dbMsgs;
    let dbMsgsPrivate;
    let dbUsers;
    let dbPrivateUsers;
    let dbHistoric;
    return {
        connect: function () {
            return new Promise((resolve, reject) => {
                let p1 = this.connectionMsgs();
                let p2 = this.connectionPrivateMsgs();
                let p3 = this.connectionUsers();
                let p4 = this.connectionBlacklist();
                Promise.all([p1, p2, p3, p4]).then(() => {
                    resolve();
                }).catch(function (e) {
                    console.log('resolveAllConnections:' + e);
                });
            });
        },
        connectionMsgs: function () {
            return new Promise((resolve, reject) => {
                MongoClient.connect(env.db.msgs.connectionString, function (err, connectionData) {
                    if (err) {
                        console.log('connectionMsgs');
                        console.error(err);
                        reject(err);
                    } else {
                        dbMsgs = connectionData.db(env.db.msgs.name); // once connected, assign the connection to the global variable
                        resolve();
                    }
                })
            });
        },
        connectionPrivateMsgs: function () {
            return new Promise((resolve, reject) => {
                MongoClient.connect(env.db.privates.connectionString, function (err, connectionData) {
                    if (err) {
                        console.log('connectionPrivateMsgs');
                        console.error(err);
                        reject(err);
                    } else {
                        dbMsgsPrivate = connectionData.db(env.db.privates.name); // once connected, assign the connection to the global variable
                        resolve();
                    }
                })
            });
        },
        connectionUsers: function () {
            return new Promise((resolve, reject) => {
                MongoClient.connect(env.db.users.connectionString, function (err, connectionData) {
                    if (err) {
                        mongodb: console.log('connectionUsers');
                        console.error(err);
                        reject(err);
                    }
                    else {
                        dbUsers = connectionData.db(env.db.users.name); // once connected, assign the connection to the global variable
                        resolve();
                    }
                })
            });
        },
        connectionBlacklist: function () {
            return new Promise((resolve, reject) => {
                MongoClient.connect(env.db.blacklist.connectionString, function (err, connectionData) {
                    if (err) {
                        console.log('blacklist');
                        console.error(err);
                        reject(err);
                    }
                    dbBlacklist = connectionData.db(env.db.blacklist.name); // once connected, assign the connection to the global variable
                    resolve();
                })
            });
        },
        getDbMsgs: function () {
            return dbMsgs;
        },
        getDbMsgsPrivate: function () {
            return dbMsgsPrivate;
        },
        getDbUsers: function () {
            return dbUsers;
        },
        getDbPrivateUsers: function () {
            return dbPrivateUsers;
        },
        getDbBlacklist: function () {
            return dbBlacklist;
        },
    }
}