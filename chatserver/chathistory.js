// that our model of public user list to give to our application
function Msgs() {
    this.from;
    this.msg;
    this.timestamp = Date.now();
}

// that's our model of private user list just to use in server and do validations
function PrivateMsgs() {
    this.from;
    this.mine;
    this.msg;
    this.video;
    this.audio;
    this.image;
    this.timestamp = Date.now();
}

exports = module.exports = function (_users, _dbMsgs) {
    var _chatsMain = [];
    var _chatsPrivate = [];
    return {
        add: function (msgObj) {
            return new Promise((resolve, reject) => {
                if (msgObj.toUser) {
                    _dbMsgs.insert(msgObj, '_chatsPrivate').then(() => {
                        resolve();
                    }).catch(function (e) {
                        console.log('insertPrivateMsg:' + e);
                    });
                } else {
                    if (msgObj.from.length <= 20) {
                        _dbMsgs.insert(msgObj, '_chatsMain').then(() => {
                            resolve();
                        }).catch(function (e) {
                            console.log('insertMainMsg:' + e);
                        });
                    }
                }
            });
        },
        getAll: function (index, ignoredList) {
            return new Promise((resolve, reject) => {
                if (index != 'max') {
                    this.getRange(index).then((range) => {
                        _dbMsgs.getMessages('_chatsMain', range, ignoredList).then((msgObj) => {
                            resolve(this.setIndex(range, index, msgObj.msgs, msgObj.dbLength));
                        }).catch(function (e) {
                            console.log('get all getMainMsgs call:' + e);
                        });
                    }).catch(function (e) {
                        console.log('getAll getRange call:' + e);
                    });
                } else {
                    resolve({
                        index: 'max',
                        msgs: _dbMsgs.getMessages('_chatsMain')
                    });
                }
            });
        },
        getPrivates: function (fromUser, toUser, token, index) {
            return new Promise((resolve, reject) => {
                var storedFromToken = _users.getUserToken(fromUser);
                var storedToToken = _users.getUserToken(toUser);
                var response = [];
                Promise.all([storedFromToken, storedToToken]).then((values) => {
                    storedFromToken = values[0];
                    storedToToken = values[1];
                    if (token === storedFromToken || token === storedToToken) {
                        this.getRange(index).then((range) => {
                            _dbMsgs.getPrivateMessages('_chatsPrivate', range, storedFromToken, storedToToken).then((msgObj) => {
                                if (msgObj.msgs) {
                                    let result;
                                    for (i = 0; i < msgObj.msgs.length; i++) {
                                        result = this.mapResponseToUser(token, fromUser, toUser, storedFromToken, storedToToken, msgObj.msgs[i]);
                                        if (result) {
                                            response.push(result);
                                        }
                                    }
                                } else {
                                    resolve([]);
                                }
                                resolve(this.setIndex(range, index, response, msgObj.dbLength));
                            }).catch(function (e) {
                                console.log('getPrivates getPrivateMsgs call:' + e);
                            });
                        }).catch(function (e) {
                            console.log('getPrivates getRange call:' + e);
                        });
                    } else {
                        resolve([]);
                    }
                });
            });
        },
        getRange: function (index) {
            return new Promise((resolve, reject) => {
                if (!index) {
                    index = 0;
                }
                let range = 0;
                for (let i = 0; i <= index; i++) {
                    range = 500 * (i + 1);
                }
                resolve(range);
            });
        },
        setIndex: function (range, index, msgs, dbLength) {
            if (range >= dbLength) {
                return {
                    index: 'max',
                    msgs: msgs
                }
            } else {
                return {
                    index: index + 1,
                    msgs: msgs
                }
            }
        },
        getLastMessage: function () {
            return new Promise((resolve, reject) => {
                _dbMsgs.getLastMessage('_chatsMain').then((msg) => {
                    resolve({
                        msg: msg
                    })
                }).catch(function (e) {
                    console.log('getLastMsg:' + e);
                });
            });
        },
        getLastPrivate: function (fromUser, toUser, token) {
            return new Promise((resolve, reject) => {
                var storedFromToken = _users.getUserToken(fromUser);
                var storedToToken = _users.getUserToken(toUser);
                Promise.all([storedFromToken, storedToToken]).then((values) => {
                    storedFromToken = values[0];
                    storedToToken = values[1];
                    if (token === storedFromToken || token === storedToToken) {
                        _dbMsgs.getLastPrivateMessage('_chatsPrivate', storedFromToken, storedToToken).then((field) => {
                            let msg = this.mapResponseToUser(token, fromUser, toUser, storedFromToken, storedToToken, field[0]);
                            if (msg) {
                                resolve({
                                    msg: msg
                                });
                            } else {
                                resolve([]);
                            }

                        }).catch(function (e) {
                            console.log('getLastPrivateMsg:' + e);
                        });
                    } else {
                        resolve([]);
                    }
                });
            });
        },
        declinedStream: function (streamID) {
            _dbMsgs.declinedStream(streamID);
        },
        mapResponseToUser: function (token, fromUser, toUser, storedFromToken, storedToToken, field, send) {
            let responseObject = {
                from: '',
                msg: ''
            }
            let fieldProp = Object.keys(field);
            for (let i = 0; i < fieldProp.length; i++) {
                if (fieldProp[i] == 'toUser') {
                    if (token === field.toUser) {
                        responseObject.from = toUser;
                        responseObject.mine = token === storedToToken;
                    } else {
                        responseObject.from = fromUser;
                        responseObject.mine = token === storedFromToken;
                    }
                    if (responseObject.mine && field.streamQuery && !send) {
                        return;
                    }
                } else {
                    responseObject[fieldProp[i]] = field[fieldProp[i]];
                }
            }
            return responseObject;
        }
    };
}