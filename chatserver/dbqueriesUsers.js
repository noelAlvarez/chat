exports = module.exports = function (dbConnection, dbMsgs) { //000000
    let dbUsers = dbConnection.getDbUsers();
    return {
        getUsers: function () {
            return new Promise((resolve, reject) => {
                dbUsers.collection('_users').find({}).toArray((err, users) => {
                    if (err) {
                        console.log('getUsers');
                        console.error(err);
                        reject(err);
                    }
                    resolve(users);
                })
            })
        },
        getOnlineUsers: function () {
            return new Promise((resolve, reject) => {
                dbUsers.collection('_users').find({
                    'online': true
                }).toArray((err, users) => {
                    if (err) {
                        console.log('getUsers');
                        console.error(err);
                        reject(err);
                    }
                    resolve(users);
                })
            })
        },
        getUserByToken: function (token) {
            return new Promise((resolve, reject) => {
                dbUsers.collection('_users').find({
                    'token': token
                }).toArray((err, user) => {
                    if (err) {
                        console.log('getUsers');
                        console.error(err);
                        reject(err);
                    }
                    resolve(user[0]);
                })
            })
        },
        insertUser: function (user) {
            return new Promise((resolve, reject) => {
                dbUsers.collection('_users').insertOne(user, (err, res) => {
                    if (err) {
                        console.log('insertUser');
                        console.error(err);
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
        },
        deleteUser: function (user) {
            dbUsers.collection('_users').remove({
                    'nick': user.nick
                },
                true,
                (err, res) => {
                    if (err) {
                        console.log('deleteUser');
                        console.error(err);
                    }
                }
            )
        },
        updateUser: function (oldNick, newNick) {
            dbUsers.collection('_users').update({
                'nick': oldNick
            }, {
                $set: {
                    'nick': newNick
                }
            });
            dbUsers.collection('_users').update({
                'ignoredUsers': oldNick

            }, {
                $set: {
                    'ignoredUsers.$': newNick
                }
            }, {
                multi: true
            });
            this.replaceNick('_chatsMain', oldNick, newNick);
        },
        updateUsers: function (users) {
            dbUsers.collection('_users').update({
                users
            }, {
                multi: true
            });
        },
        setConnectionState: function (nick, state) {
            return new Promise((resolve, reject) => {
                let lastConnection;
                if (!state) {
                    lastConnection = Date.now();
                } else {
                    lastConnection = null;
                }
                dbUsers.collection('_users').update({
                    'nick': nick
                }, {
                    $set: {
                        'online': state,
                        'lastConnection': lastConnection
                    }
                }, {
                    multi: true
                }, function (res) {
                    resolve();
                });
            });
        },
        manageIgnored: function (token, ignoredName) {
            dbUsers.collection('_users').find({
                'token': token,
                'ignoredUsers': [ignoredName]

            }).toArray((err, user) => {
                if (err) {
                    console.log('manageIgnored');
                    console.error(err);
                    reject(err);
                } else {
                    if (user.length > 0) {
                        this.deleteIgnored(token, ignoredName);
                    } else {
                        this.insertIgnored(token, ignoredName);
                    }
                }
            });
        },
        insertIgnored: function (token, ignoredName) {
            dbUsers.collection('_users').update({
                'token': token
            }, {
                $push: {
                    'ignoredUsers': ignoredName
                },
            });
        },
        deleteIgnored: function (token, ignoredName) {
            dbUsers.collection('_users').update({
                'token': token
            }, {
                $pull: {
                    'ignoredUsers': ignoredName
                },
            });
        },
        getUser: function (userName) {
            return new Promise((resolve, reject) => {
                dbUsers.collection('_users').find({
                    'nick': userName
                }).toArray((err, user) => {
                    if (err) {
                        console.log('getUsers');
                        console.error(err);
                        reject(err);
                    }
                    resolve(user[0]);
                })
            })
        },
        replaceNick: function (table, oldNick, newNick) {
            dbMsgs.replaceNick(table, oldNick, newNick);
        },
        deleteOldUsers: function () {
            //remove old users with 2 months of inactivity (5256000000)
            setInterval(() => {
                this.getUsers().then((_users) => {
                    for (i = Object.getOwnPropertyNames(_users).length - 1; i >= 0; --i) {
                        let userName = Object.getOwnPropertyNames(_users)[i];
                        if (_users[userName].lastConnection && (Date.now() - _users[userName].lastConnection) >= 5256000000) {
                            let userToken = _users[userName].token;
                            let privateMsgs = dbMsgs.deletePrivateMessages(userToken).then(() => {
                                this.deleteUser(_users[userName]);
                            }).catch(function (e) {
                                console.log('deleteOldUsers deletePrvMsgs call:' + e);
                            });
                        }
                    };
                })
            }, 5256000000);
        },
    }
}